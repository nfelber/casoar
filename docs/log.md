# Casoar Log
## Unified Human Gastrointestinal Genome (UHGG)
Downloaded the genomes metadata following [this link](http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/genomes_metadata.tsv).

Data is stored in `.tsv` format, as shown in this table with the first entry (280,000+ in total):

| field            | value |
| :--------------- | :---- |
| Genome           | GUT_GENOME000001 |
| Original_name    | 11861_6_55 |
| Study_set        | HBC |
| Genome_type      | Isolate |
| Length           | 3221441 |
| N_contigs        | 137 |
| N50              | 47272 |
| GC_content       | 28.26 |
| Completeness     | 98.59 |
| Contamination    | 0.7 |
| rRNA_5S          | 88.24 |
| rRNA_16S         | 99.74 |
| rRNA_23S         | 99.83 |
| tRNAs            | 20 |
| Genome_accession | NA |
| Species_rep      | GUT_GENOME000001 |
| MGnify_accession | MGYG-HGUT-00001 |
| Lineage          | d__Bacteria;p__Firmicutes_A;c__Clostridia;|o__Peptostreptococcales;f__Peptostreptococcaceae;g__GCA-900066495;s__ |
| Sample_accession | ERS370061 |
| Study_accession  | ERP105624 |
| Country          | United Kingdom |
| Continent        | Europe |
| FTP_download     | ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/all_genomes/MGYG-HGUT-000/MGYG-HGUT-00001/genomes1/GUT_GENOME000001.gff.gz |

Extracted every entry with field `Genome_type` = `Isolate` (cultured genomes) with `mlr` ([Miller](johnkerl.org/miller/doc/index.html)):
```console
$ mlr --tsv filter '$Genome_type == "Isolate"' genomes_metadata.tsv > isolates.tsv
```

Getting the number of isolate genomes (double check):

```console
$ mlr --itsv --opprint cut -f Genome_type then uniq -a -c isolates.tsv
count Genome_type
10648 Isolate
$ tail -n+2 isolates.tsv | wc -l
   10648
```

Downloading every `.gff.gz` file following the FTP links in the `FTP_download` fields with `wget`:
```console
$ mlr --itsv --onidx cut -f FTP_download < isolates.tsv | wget -i -
```

Unzipping each of these files:

```console
$ gunzip -rv genomes/
```

These files actually contain `.gff` and `.fasta` data delimited by **##FASTA**:

```
...
GUT_GENOME000001_125	barrnap:0.9	rRNA	360	461	6e-16	+	.	ID=GUT_GENOME000001_03274;locus_tag=GUT_GENOME000001_03274;product=5S ribosomal RNA
GUT_GENOME000001_127	Aragorn:1.2	tRNA	1	88	.	-	.	ID=GUT_GENOME000001_03275;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03275;product=tRNA-Leu(taa)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	124	197	.	-	.	ID=GUT_GENOME000001_03276;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03276;product=tRNA-Cys(gca)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	205	280	.	-	.	ID=GUT_GENOME000001_03277;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03277;product=tRNA-Lys(ttt)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	289	365	.	-	.	ID=GUT_GENOME000001_03278;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03278;product=tRNA-His(gtg)
##FASTA
>GUT_GENOME000001_1
CATAACATTTCTTTTCTTATAAATAAATTAATGGGTAAAAATGACTCTTTAGCTAAAATA
AAAGAACTAGTATTTGGATAAAATTTACAAATACTAGTCACTATGGGTCTATTTTTTTGC
AAAAAATATTTTACTTTATTTGGAAATTAAATTTAACTAGCACAATAGATTAATATATAT
AATTTTTTTATAACAGAGGAGATAGTAGTCTAGAAATAGCTTCTTTTATTTTGATAGATC
...
```

Breaking down each genome data file into a `.gff` and a `.fasta` file according to the **##FASTA** delimiter:

```console
$ for filename in $(ls genomes); do sed -n '/##FASTA/,$p' < genomes/$filename | tail -n+2 > $(echo genomes/$filename | sed 's/.gff/.fasta/'); done
$ for filename in $(ls genomes | sed -n '/.gff/p'); do sed -n '1,/##FASTA/p' < genomes/$filename | sed '$d' > genomes/$filename.tmp; mv genomes/$filename.tmp genomes/$filename; done
```

## MEGARes: an Antimicrobial Database for High-Throughput Sequencing

Downloaded all 3 different files through [this link](https://megares.meglab.org/download/index.php).

### 1) megares_database_v1.01.fasta:

First sequence:

```
>Bla|OXA-223|JN248564|1-825|825|betalactams|Class_D_betalactamases|OXA
ATGAACATTAAAACACTCTTACTTATAACAAGCGCTATTTTTATTTCAGCCTGCTCACCTTATATAGTGACTGCTAATCCAAATCACAGCGCTTCAAAATCTGATGAAAAAGCAGAGAAAATTAAAAATTTATTTAACGAAGTACACACTACGGGTGTTTTAGTTATCCAACAAGGCCAAACTCAACAAAGCTATGGTAATGATCTTGCTCGTGCTTCGACCGAGTATGTACCTGCTTCGACCTTCAAAATGCTTAATGCTTTGATCGGCCTTGAGTACCATAAGGCAACCACCACAGAAGTATTTAAGTGGGACGGGCAAAAAAGGCTATTCCCAGAATGGGAAAAGGACATGACCCTAGGCGATGCTATGAAAGCTTCCGCTATTCCGGTTTATCAAGATTTAGCTCGTCGTATTGGACTTGAACTCATGTCTAAGGAAGTGAAGCGTGTTGGTTATGGCAATGCAGATATCGGTACCCAAGTCGATAATTTTTGGCTGGTGGGTCCTTTAAAAATTACTCCTCAGCAAGAGGCACAATTTGCTTACAAGCTAGCTAATAAAACGCTTCCCTTTAGCCCAAAAGTCCAAGATGAAGTGCAATCCATGCTATTCATAGAAGAAAAGAATGGAAATAAAATATACGCAAAAAGTGGTTGGGGATGGGATGTAAACCCACAAGTAGGCTGGTTAACTGGATGGGTTGTTCAGCCTCAAGGAAATATTGTAGCGTTCTCCCTTAACTTAGAAATGAAAAAAGGAATACCTAGCTCTGTTCGAAAAGAGATTACTTATAAAAGTTTAGAACAATTAGGTATTTTATAG
```
### 2) megares_annotations_v1.01.csv:

First entry:

| field | value |
| :---- | :---- |
| header    | Bla\|OXA-223\|JN248564\|1-825\|825\|betalactams\|Class_D_betalactamases\|OXA |
| class     | betalactams |
| mechanism | Class D betalactamases |
| group     | OXA |

### 3) megares_to_external_header_mappings_v1.01.tsv:

First entry:

| field | value |
| :---- | :---- |
| Source_Database                 | ARG-ANNOT |
| MEGARes_Header                  | Bla\|OXA-223\|JN248564\|1-825\|825\|betalactams\|Class_D_betalactamases\|OXA |
| Source_Headers(space_separated) | (Bla)OXA-223:JN248564:1-825:825 |

## BLAST : Basic Local Alignment Search Tool

[BLAST website](https://blast.ncbi.nlm.nih.gov/Blast.cgi).

Appended all `.fasta` genomic sequence files in one single file (all_genomes.fasta):

```console
$ for f in $(ls | grep .fasta); do cat $f >> all_genomes.fasta; done
```

Making the BLAST database:

```console
$ makeblastdb -in all_genomes.fasta -dbtype nucl
```

Performing BLASTN with the MEGARes database on our database:

```console
$ blastn -query ../megares_v1.01/megares_database_v1.01.fasta -db all_genomes.fasta -outfmt 6 -out blast_out.tsv
```

Headers can be found [here](http://www.metagenomics.wiki/tools/blast/blastn-output-format-6).

## Join

Preparing `blast_out.tsv` for join by adding genome entry.

```console
$ mlr --tsv put '$sseqid =~ "GUT_GENOME([0-9]+)_([0-9]+)"; $genome = "GUT_GENOME\1"' blast_out.tsv > blast_out_wGenome.tsv
```

Perform a join operation on `genomes_metadata.tsv` and `megares_annotations_v1.01.csv` using `blast_out_wGenome.tsv` information:

```console
$ python3 join.py --inline 'c0-b0 c1 c2 c10 c11 "arg_class"b2 "gen_lineage"a17 c12-a0' join_out.tsv isolates.tsv megares_annotations_v1.01.csv blast_out_wGenome.tsv
```

Extracting the phylum from gen_lineage and adding it as an entry of `join_out.tsv`

```console
$ mlr --tsv put '$gen_lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' join_out.tsv > join_out_wPhylum.tsv
```

`join_out_wPhylum.tsv`:

| field | value |
| :---- | :---- |
|qseqid | Tet\|TetO\|M18896\|207-2126\|1920\|Tetracyclines\|Tetracycline_resistance_ribosomal_protection_prot |
|sseqid | GUT_GENOME000003_11 |
|pident | 99.792 |
|evalue | 0.0 |
|bitscore | 3524 |
|arg_class | Tetracyclines |
|gen_lineage | d__Bacteria;p__Firmicutes_A;c__Clostridia;o__Lachnospirales;f__Lachnospiraceae;g__Blautia_A;s__Blautia_A wexlerae |
|genome | GUT_GENOME000003 |
|phylum | Firmicutes |

## Heatmap with R script

```console
$ Rscript heatmap.r join_out_wPhylum.tsv
```

To get a `.png` version from `pdf`:

```console
$ convert -density 150 Rplots.pdf -quality 90 Rplots.png
```

`Rplots.png`

![heatmap.r output example](/home/nf/maturity_project/casoar/src/Rplots.png "Rplots.png")
