---
linkcolor: blue
---

My comments appear as block quotes, i.e. shifted to the right - TJ. Typos to fix
appear in **boldface**, things to remove in ~~strikeout~~. (I could have fixed
them myself, but I thought it was more didactic if I pointed them out :-).


# Intro

> I'd spell it out: Introduction

## Antibiotics

Antibiotics (literally "against life") are antimicrobial substances widely used
in medicine for antibacterial treatments. An antibiotic will act against a
defined range of targets, either by killing them, or by inhibiting their growth.

Antibiotics came as a revolution when Alexander Fleming discovered modern day
penicillin in 1928. Accessibility of antibiotics in term of price and

> Not sure that 'modern day' is really needed. Fleming discovered penicillin,
> plain and simple, today's penicillin is the same as that of 1924 (although
> some chemical derivatives exist).

availability encouraged an excessive use of them. However, this revolution,

> I wouldn't say 'However', because this word introduces an opposition or
> concession, while your next sentence elaborates rather than opposing: the
> major issue is a _consequence_ of the excessive use, not a moderating or
> opposing phenomenon. Hence, I'd use another link word. How about this:

> > Accessibility of antibiotics ... encouraged
> > an excessive use of them. _Indeed_, this revolution ...
> > quickly appeared to have a major issue. 

which made it possible to cure previously deadly diseases and saved so many
lives, quickly appeared to have a major issue. Targeted bacteria can evolve to
fight these antibiotics by developing antibiotic resistance mechanisms.[AMR](https://amr-review.org/)

With these antibiotic resistance mechanisms spreading, specific antibiotics
become less and less efficient. Fortunately, these resistance mechanisms come at
a price for the bacteria, be it a need for energy, or a weakening in other
defensive systems. It implies that these resistances will not appear without
selective pressure from the environments, and will gradually disappear with time
if they were previously present.

> Quite right; I'd just maybe state explicitly that the resistances will
> disappear _if selective pressure by antibiotics is not maintained_ (if
> selection persists, then resistance will likely be advantageous, whatever its
> cost)..

What distinguish**es** antibiotics from other antimicrobial**s** is that
antibiotics are naturally produced in the environment, by microorganisms as a
mean**s** of defense.  In this project we are particularly interested about

> Antibiotics also serve as signaling molecules - perhaps worth mentioning. 

antibiotics, because they encounter natural resistance from organisms which have
adapted themselves.

> I'd phrase slightly differently: ... because they encounter resistance through
> the natural adaptation of organisms. 
> But that's only a matter of taste, your phrase is not wrong.§

Precisely, resistance mechanisms are naturally present in environments where
antibiotics are found (produced by microorganisms). In such environments, the
medical use of antibiotics will increase selective pressure, and great

> "great" is perhaps a bit vague or slightly emphatic. Maybe "strong" would be
> more neutral. Or perhaps just say that resistance will increase.

resistance will appear. Knowledge about the presence of specific antibiotic
resistances in different environments is crucial to predict what impact ~~will~~ a
given antibiotic **will** have when used on them.

Resistance mechanisms appear as the result of random genetic mutations that will
be selected in case of pressure. Thus, we can detect them by analyzing the
genetic information of the desired species. The genes responsible for resistance
are called Antibiotic Resistance Genes (ARGs).

Target antibiotics and mechanisms of a large number of ARGs have already been
studied and compiled into databases [Doster et al. 2019 ]. We can determine that a species
possesses a given resistance if we can find a sequence of its genome

> I'd say "is likely to possess" instead of "possesses". The reasons are as
> follows: (i) some BLAST hits are not perfect (in fact, few are), which still
> leaves open the possibility that a gene in one of our genomes has a _different
> function_ (perhaps it destroys a similar yet different chemical than the
> antibiotic) from the ARG, even if it is very similar to it at the sequence
> level; (ii) even if the ARG is _present_ in the genome, it may not be
> _expressed_: think of a binary in `/usr/local/bin` that is there but never
> executed.

significantly similar to the DNA sequence of the corresponding ARG.

Genetic data of bacterial genomes have also been sequenced and compiled into

> You can shorten this to "Bacterial genomes have also been sequenced..."

more and more complete databases [REF] thanks to high throughput-squencing
[REF](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3831009/), shotgun
metagenomics methods [REF](https://www.nature.com/articles/nbt.3935) and
progress in microbiological culture, giving everyone access to tremendous
amounts of information. In our project, we focused on genomes sequenced from
cultured bacteria. This has the effect of limiting the diversity of the
organisms we studied, but the advantage of dealing with more complete and
precise data, which is still representative of the whole microbiota.

In the last years, computer science has brought the power of algorithms [REF]

> Maybe qualify this a little. The first bioinformatics algorithms were
> published in the '70s, but it's quite true that only in the past two decades
> or so did researchers start to have the computing power needed for projects
> like this one.

and data analysis to modern days biology. Thanks to these technologies and the
availability of large databases [REF], we are able to parse remarkably large
amounts of data in little time, and thus explore a whole world of information we
could not cope with otherwise.

For this project, we studied the human gastrointestinal microbiota, in order to
figure out what resistances were present. 

> I'd say "determine" instead of "figure out", which is a little colloquial.

All the genetic code we used comes

> The phrase "genetic code" has a technical meaning different from what you mean
> here: it is a mapping from DNA codons to amino acids. In the present case, I'd
> say "genomic sequence".

from cultured organisms, themselves coming from healthy adult human donors.
Thus, we have good reasons to think that antibiotics have not directly had an
selective impact in these microorganisms populations.

Our hypothesis is that there exist natural antibiotic resistances in the human
gastrointestinal microbiota, and that we can find which ones are present and in
what proportion by searching for antibiotic resistance genes in healthy human
gastrointestinal isolates.

> I like this. Referring to our discussion of hyposis-based versus
> non-hypothesis-based science, you have found a way to specify a hypothesis for
> the project, even when I wasn't so sure.

In order to test this hypothesis, we have been following a precise method. We

> You don't need to actually say this (even if, of cousre, it is true): every
> scientific project follows a precise method (even though it's not always well
> defined in advance). What would be really worth saying (and quite surprising)
> would be if a science project did _not_ follow some method. But then, I doubt
> it would find a publisher... So you might prefer something like

> > In order to test this hypothesis, we first found two databases ...

first found two databases [REF] containing annotated DNA sequences, one from
10,648 cultured human gastrointestinal microorganisms, and the other one from
7,868 antimicrobial resistance genes. Then we looked for the antimicrobial
resistance genes in the human gastrointestinal microorganisms isolates, and
listed every hit. After that, we took every one of these hits and gathered them
by phylum, for easier visualization. Then, we had to count the number of
antimicrobial resistance genes found inside each phylum and gather them by
class, once again, for the sake of visualization. In the end, we displayed the
results in a two-dimensional heatmap.

As previously stated, we limited ourselves to the distinction of bacteria by
phylum and to the the study of genomes from cultured microorganisms. The limits
of this project are further examined in the discussion section [REF].

# Method

> It is more common to use the plural: Methods

## **D**atabases

For our project, we used the two following databases:

### Unified Human Gastrointestinal Genome (UHGG)

The Unified Human Gastrointestinal Genome (UHGG) collection
[REF](https://www.biorxiv.org/content/10.1101/762682v1.full) combines annotated
genetic sequences from 286,997 genomes found in the human gut, 10,648 of which
are isolates.

We downloaded the `genomes_metadata.tsv` file following [this
link](http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/genomes_metadata.tsv).
This `.tsv` file contains one record per genome, each of which contains
information about the lineage, whether it is an isolate or a
metagenome-assembled genomes (MAG)
[REF](https://www.nature.com/articles/s41598-019-39576-6), which is
culture-independant, and an FTP link towards the DNA sequences and more (cf.
appendix 1).

### MEGARes: an Antimicrobial Database for High-Throughput Sequencing

[MEGARes](https://megares.meglab.org/)
[REF](https://academic.oup.com/nar/article/48/D1/D561/5624973) is a database
gathering DNA sequences from 7,868 antimicrobial resistance genes along with
annotations.

We downloaded all the different files following [this
link](https://megares.meglab.org/download/megares_v2.00.zip). We used two of the
files found in this directory: `megares_modified_database_v2.00.fasta`, which
contains the DNA sequences matched to a header, and
`megares_modified_annotations_v2.00.csv`, which displays information about the
classes, mechanisms and groups of the antimicrobial resistance genes (cf.
appendices 2 & 3).

## Preparing the data from the Unified Human Gastrointestinal Genome database

First, we extracted every record of the file `genomes_metadata.tsv` with the
field `Genome_type` equal to `Isolate`. We created a new `isolates.tsv` file
with the metadata of these 10,648 isolates. To do so, we used a program called
[Miller](johnkerl.org/miller/doc/index.html).

```console
$ mlr --tsv filter '$Genome_type == "Isolate"' genomes_metadata.tsv > isolates.tsv
$ mlr --itsv --opprint count-distinct -f Genome_type isolates.tsv
count Genome_type
10648 Isolate
```

We then followed the FTP links under the field `FTP_download` of our
`isolates.tsv` file and downloaded the corresponding `.gff.gz` file into a
directory called `genomes` using `mlr` and `wget`. As these files are
compressed, th**e**y need to be unzipped using `gunzip`. The total volume of the
downloaded files is about 50 GB.

```console
$ mlr --itsv --onidx cut -f FTP_download < isolates.tsv | wget -P genomes -i -
$ gunzip -rv genomes
```

All the downloaded `.gff` files actually contain `.gff` and `.fasta` data
delimited by a single line where a **##FASTA** tag stand. We could practically
not work with these half-half files, so we had to split the files into a `.gff`
and a `.fasta` version, using combinations of `sed` commands [REF].

```console
$ for filename in $(ls genomes); do sed -n '/##FASTA/,$p' < genomes/$filename | sed '1d' > $(echo genomes/$filename | sed 's/.gff/.fasta/'); done
$ for filename in $(ls genomes | sed -n '/.gff/p'); do sed -n '1,/##FASTA/p' < genomes/$filename | sed '$d' > genomes/$filename.tmp; mv genomes/$filename.tmp genomes/$filename; done
```

As we will not use the `.gff` data during the project, we can simply delete them
from the files instead of splitting them in two. So we can run this command
instead of the two previous.

```console
$ for filename in $(ls genomes); do sed -i '1,/##FASTA/d' genomes/$filename; done
```

## BLAST : Basic Local Alignment Search Tool

[BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi) [REF] is a program that has
become a standard in biology to find regions of similarity between DNA

> BLAST also works on protein sequences, so you could either (i) drop DNA and
> say "similarity between sequences" (or "biological sequences" if you think it
> won't be clear enough to the jury members), or (ii) explicitly mention
> proteins "similarity between DNA or protein sequences".

sequences. We used it to find correlations between our UHGG genomes and the
antimicrobial resistance genes of MEGARes, in order to detect the presence of
antimicrobial resistance in these isolates.

BLAST needs one single `.fasta` file with all the genetic information sequences.

> More accurately, BLAST needs one _query_ sequence as well as a _database_ of
> sequences in which matches of the query will be searched for. Several query
> sequences can be put together in a file, they will be BLASTed in turn against
> the same database.

So we first appended all the `.fasta` genomic sequence files inside one single
file that we called `all_genomes.fasta`.

```console
$ for filename in $(ls genomes | grep .fasta); do cat genomes/$filename >> all_genomes.fasta; done
```

Be careful, as it will copy all of the `.fasta` data (about 42 GB), thus
doubling the volume needed on the disk. A simple solution consists of deleting
the individual files as they are appended to `all_genomes.fasta` instead.

> In saying "Be careful", you're giving hints or instructions. This is ok for a
> manual, tutorial or HOWTO; however, in the case of an article or report,
> readers are expecting to learn what you did rather than to be told how to
> proceed. You should therefore signal potential problems more indirectly, for
> example by commenting on any non-obvious steps. So I would say instead:

> > The original fasta files were then deleted in order so save space.

```console
$ for filename in $(ls genomes | grep .fasta); do cat genomes/$filename >> all_genomes.fasta; rm genomes/$filename; done
```

In order to run, BLAST needs to make a database with all the genomes in advance
with the command `makeblastdb`.

> See my comment above regarding what BLAST needs to run. The reader will now
> know that BLAST needs a database, so you can directly say e.g.:

> > To make the BLAST database, we ran the following command

```console
$ makeblastdb -in all_genomes.fasta -dbtype nucl
```

~~This will produce a number of `.nhr`, `.nin` and `.nsq` files in the working
directory.~~

> You can leave this out because it doesn't really explain what you did and why
> - it's just the way BLAST works, and your readers will most probably either
> already know this (if they know some bioinformatics) or not really want so
> much details (most other people... :-)

Finally, we performed a `blastn` (nucleotide BLAST) command to look for
antimicrobial resistances from the MEGARes fasta data in the genomes of the UHGG
database. We stored the output in `.tsv` format in a file called
`blast_out.tsv`. Before running the command, we needed to prepare the output
file with its headers, which can be found
[here](http://www.metagenomics.wiki/tools/blast/blastn-output-format-6), as they
are not automatically created by `blastn`.

```console
$ echo '1iqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore' > $OUT_DIR/blast_out.tsv
$ blastn -query megares_v2.00/megares_modified_database_v2.00.fasta -db all_genomes.fasta -outfmt 6 >> blast_out.tsv
```

## Join : put our data together

In data science, the join operation consists of merging two datasets together by

> You may want to put the word "join" in italics, as you're introdcing a new and
> important concept:

> > In data science, the _join_ operation ...

combining records which contain the same value inside a specified field.

Here, each record of the `blast_out.tsv` file (cf. appendix 5) represents a hit

> I'd define th term _hit_ before using it. You might say "represents a region
> of similarity between the query and a database sequence. Such regions are
> referred to as _hits_. ..."
> In fact, you may even wish to avoid using the term _hit_ in the introduction,
> or else define it there.

between an antimicrobial resistance gene and a region of bacterial genome. They
possess a field with header `1iqseqid` corresponding the `header` section of the
file `megares_modified_annotations_v2.00.csv` (cf. appendix 3) and a field with
header `sseqid` corresponding the `Genome` section of the file
`genomes_metadata.tsv` (cf. appendix 1), along with additional information about
the similarity of the matched sequences.

However, the values of the `sseqid` section of `blast_out.tsv` does not exactly
correspond to the values of the `Genome` section of `genomes_metadata.tsv`,
because the former precise the DNA region where similarity with the ARG was
found (GUT_GENOME096550 turns into GUT_GENOME096550_18). In order to join the
two files, we must ensure the the two values are equal, and thus we created a
new file called `blast_out_wGenome.tsv` with an additional field containing the
name of the genome without the part indicating the DNA region (cf. appendix 6).

```console
$ mlr --tsv put '$sseqid =~ "GUT_GENOME([0-9]+)_([0-9]+)"; $genome = "GUT_GENOME\1"' blast_out.tsv > blast_out_wGenome.tsv
```

Then we merged `genomes_metadata.tsv` and `megares_annotations_v1.01.csv` using
`blast_out_wGenome.tsv` to make the link between them. To do so, we used a
custom join program written in python called `join.py` available in the `src/`
directory of our GitLab repository (cf. appendix A). The result is a `.tsv` file
called join_out.tsv (cf. appendix 7).

```console
$ python3 join.py --inline 'c0-b0 c1 c2 c10 c11 "arg_class"b2 "gen_lineage"a17 c12-a0' join_out.tsv isolates.tsv megares_annotations_v1.01.csv blast_out_wGenome.tsv
```

In `join_out.tsv`, each record corresponds to a hit of BLAST, and they contain
one field for the antimicrobial class of the resistance gene, called
`arg_class`, and one field for the lineage of the genome where the resistance
was found, called `gen_lineage`. However, the latest contains the whole lineage,
while we only need the name of the phylum. So we added a field called `phylum`
to each record and filled it up with the name of the phylum extracted from the
genome lineage. We stored the output in a file named `join_out_wPhylum.tsv` (cf.
appendix 8).

```console
$ mlr --tsv put '$gen_lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' join_out.tsv > join_out_wPhylum.tsv
```

## Display the results in a Heatmap using R

In order to visualize our results, we used the R programming language
[REF](https://www.r-project.org/) to produce a two-dimensional heatmap with
normalized columns which display each antimicrobial resistance found for every
phylum, and in what proportion compared to the others. Our Rscript can be found
in our GitLab repository (`/src/heatmap.r`). It takes the `.tsv` file we wnat to
use as an parameter.

> You may want to give a sketch of the main analysis steps of the R script.

```console
$ Rscript heatmap.r join_out_wPhylum.tsv
```

# Results

`heatmap.png`

![heatmap.r output example](../normalized_heatmap.png "heatmap.png")

Fig 1

This heatmap (Fig. 1) displays the final results of our project. Each column is
normalized and contains the data for one phylum. Each row represents one class
of antimicrobial. Each value is a number between 0 and 1 indicating the
proportion of the corresponding antimicrobial resistance in the corresponding
phylum. These values are colored according to the scale on the top left of the
graphic.

The rows and columns are sorted according to the dendrograms which are displayed
on the top and left of the heatmap. They indicate the distance computed between
each row or column, in terms of similarity.

> For clarity (I'm thinking of those people in your jury who may not be familiar
> with dendrograms, you may want to add a sentence or two along the lines of

> > Phyla with similar resistance fractions across antibiotics appear close
> > together in the tree, and hence in the heatmap (for example, Fircmicutes and
> > Actinobacteria). Likewise, classes of antibiotics to which phyla exhibit
> > similar propportions of resistant genomes appear close together (e.g.
> > pactamycin and phenicol). Note, however, that the reverse is not always
> > true: $\beta$-lactams appear next to tetracyclins in the heatmap, but they
> > belong in different main branches of the tree. The same is true for
> > Synergistota and Campylobacterota.

> You can also point out here that phyla appear to fall into two groups: one one
> side, those with a moderate to high proportion resistance to tetracyclines
> (but not MLS or $\beta$-lactams), comprising Eryarchaeota and Synergistota, on
> the other side, all the rest (which share low to moderate resistance to
> tetracyclines and at least one of $\beta$-lactams or MLS, or show very little
> resistance to any of these three (Thermoplasmatota, Halobectrota). In the
> other dimension, you can point out that the tetracyclines are separated from
> all the other antimicrobials: many phyla who have high proportions of
> resistance to tetracyclines have low resistance to other compounds, etc.

# Discussion

The first noticeable information that our heatmap (Fig. 1) displays is that the
antimicrobials **in** ~~making~~ the upper rows ~~of our heatmap~~ are only found in
proportions close to zero, and thus none of the associated antimicrobial
resistance genes are found in any significant proportion in any of the phyla.

The scope of this research is limited to the level of the phyl**um**. Thus, it
is unclear if the inner phylum distribution of the ARGs is homogeneous.

(We do not have sufficient data) about certain phyla, which means that no
accurate conclusion can be made for them.

> An interesting point mentioned by Pilar is that some of the resistant phyla
> (namely, Euryarchaeota, Thermoplasmatota (previously considered part of
> Euryarchaeota), and Halobacterota), belong to Archaea, one of the main
> division of prokaryotes. This is important for two reasons: (i) no known human
> pathogen belongs to this group; and (ii) Archaea are actually closer to
> animals than they are to (true) bacteria, which means that antibiotics toxic
> to Archaea may have a higher risk of being toxic to us as well. It also opens
> the question of why Archaea seem to have evolved resistance to at least some
> antimicrobials.

> Finally, also mentioned by Pilar, it's important to say that the only way to
> make sure that a bacterium really is resistant to a given antimicrobial is to
> test for resistance in the laboratory (remember my point about BLAST hits not
> being as of themselves indication of resistance). This means that results like
> these serve the important function of showing which bacteria are most likely
> to exhibit resistance - even though the lab has the final word, as it were.

# Conclusion

We proved that some human gut microorganisms exhibit antibiotic resistance genes
because we obtained regions of significant similarity between genomes from
cultured human gut microorganisms, found in the Unified Human Gastrointestinal
Genome collection, and antibiotic resistance genes from the MEGARes collection
as a result of the BLAST algorithm.

In addition, we found out that some antimicrobial resistance genes are much more
present than others, as for the Tetracycline resistance. Our normalized heatmap
shows a number of resistance which are not found in significant amounts in any
of the phyla we worked on. It is the case for many non-antibiotic antimicrobial,
such as ~~the~~ **z**inc or **a**luminum resistance.

Moreover, we observed in our normalized heatmap that different antimicrobial
resistances are not homogeneously present in each of the displayed phyla, even
if they are all found in the same environment, and subject to the same selective
constraints. This means that the resistance to antibiotics not only depend on
the environment, but also on the phyla themselves (and the species, by
extension).

- Some human gut microorganisms exhibit ARGs.
  - BLAST between ARGs and cultured human microorganisms' DNA sequences.
    - Significant e-values.
- Some ARGs are much more present than others (i.e. Tetracycline resistance)
  - Some ARGs are significantly present in a large number of the tested phyla.
    - Normalized heatmap.
  - About half of the tested antibiotics find no significant resistance in any
    of the tested phyla.
    - Normalized heatmap.
- The ARGs depend on the phyla.
  - The distribution of ARGs is not homogeneous between the different tested
    phyla.

# Sources

[Wikipedia: Antibiotic](https://en.wikipedia.org/wiki/Antibiotic)

# Appendices

## 1) Record example of `genomes_metadata.tsv`

| field            | value |
| :--------------- | :---- |
| Genome           | GUT_GENOME000001 |
| Original_name    | 11861_6_55 |
| Study_set        | HBC |
| Genome_type      | Isolate |
| Length           | 3221441 |
| N_contigs        | 137 |
| N50              | 47272 |
| GC_content       | 28.26 |
| Completeness     | 98.59 |
| Contamination    | 0.7 |
| rRNA_5S          | 88.24 |
| rRNA_16S         | 99.74 |
| rRNA_23S         | 99.83 |
| tRNAs            | 20 |
| Genome_accession | NA |
| Species_rep      | GUT_GENOME000001 |
| MGnify_accession | MGYG-HGUT-00001 |
| Lineage          | d__Bacteria;p__Firmicutes_A;c__Clostridia;|o__Peptostreptococcales;f__Peptostreptococcaceae;g__GCA-900066495;s__ |
| Sample_accession | ERS370061 |
| Study_accession  | ERP105624 |
| Country          | United Kingdom |
| Continent        | Europe |
| FTP_download     | ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/all_genomes/MGYG-HGUT-000/MGYG-HGUT-00001/genomes1/GUT_GENOME000001.gff.gz |

## 2) Record example of `megares_modified_database_v2.00.fasta`

```
>Bla|OXA-223|JN248564|1-825|825|betalactams|Class_D_betalactamases|OXA
ATGAACATTAAAACACTCTTACTTATAACAAGCGCTATTTTTATTTCAGCCTGCTCACCTTATATAGTGACTGCTAATCCAAATCACAGCGCTTCAAAATCTGATGAAAAAGCAGAGAAAATTAAAAATTTATTTAACGAAGTACACACTACGGGTGTTTTAGTTATCCAACAAGGCCAAACTCAACAAAGCTATGGTAATGATCTTGCTCGTGCTTCGACCGAGTATGTACCTGCTTCGACCTTCAAAATGCTTAATGCTTTGATCGGCCTTGAGTACCATAAGGCAACCACCACAGAAGTATTTAAGTGGGACGGGCAAAAAAGGCTATTCCCAGAATGGGAAAAGGACATGACCCTAGGCGATGCTATGAAAGCTTCCGCTATTCCGGTTTATCAAGATTTAGCTCGTCGTATTGGACTTGAACTCATGTCTAAGGAAGTGAAGCGTGTTGGTTATGGCAATGCAGATATCGGTACCCAAGTCGATAATTTTTGGCTGGTGGGTCCTTTAAAAATTACTCCTCAGCAAGAGGCACAATTTGCTTACAAGCTAGCTAATAAAACGCTTCCCTTTAGCCCAAAAGTCCAAGATGAAGTGCAATCCATGCTATTCATAGAAGAAAAGAATGGAAATAAAATATACGCAAAAAGTGGTTGGGGATGGGATGTAAACCCACAAGTAGGCTGGTTAACTGGATGGGTTGTTCAGCCTCAAGGAAATATTGTAGCGTTCTCCCTTAACTTAGAAATGAAAAAAGGAATACCTAGCTCTGTTCGAAAAGAGATTACTTATAAAAGTTTAGAACAATTAGGTATTTTATAG
```
## 3) Record example of `megares_modified_annotations_v2.00.csv`

| field | value |
| :---- | :---- |
| header    | Bla\|OXA-223\|JN248564\|1-825\|825\|betalactams\|Class_D_betalactamases\|OXA |
| class     | betalactams |
| mechanism | Class D betalactamases |
| group     | OXA |

## 4) Split example between `.gff` and `.fasta` in the files downloaded from UHGG

```
...
GUT_GENOME000001_125	barrnap:0.9	rRNA	360	461	6e-16	+	.	ID=GUT_GENOME000001_03274;locus_tag=GUT_GENOME000001_03274;product=5S ribosomal RNA
GUT_GENOME000001_127	Aragorn:1.2	tRNA	1	88	.	-	.	ID=GUT_GENOME000001_03275;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03275;product=tRNA-Leu(taa)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	124	197	.	-	.	ID=GUT_GENOME000001_03276;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03276;product=tRNA-Cys(gca)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	205	280	.	-	.	ID=GUT_GENOME000001_03277;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03277;product=tRNA-Lys(ttt)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	289	365	.	-	.	ID=GUT_GENOME000001_03278;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03278;product=tRNA-His(gtg)
##FASTA
>GUT_GENOME000001_1
CATAACATTTCTTTTCTTATAAATAAATTAATGGGTAAAAATGACTCTTTAGCTAAAATA
AAAGAACTAGTATTTGGATAAAATTTACAAATACTAGTCACTATGGGTCTATTTTTTTGC
AAAAAATATTTTACTTTATTTGGAAATTAAATTTAACTAGCACAATAGATTAATATATAT
AATTTTTTTATAACAGAGGAGATAGTAGTCTAGAAATAGCTTCTTTTATTTTGATAGATC
...
```

## 5) Record example of `blast_out.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside-resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|length | 1275
|mismatch | 234
|gapopen | 26
|qstart | 247
|qend | 1507
|sstart | 1855
|send | 596
|evalue | 0.0
|bitscore | 870

## 6) Record example of `blast_out_wGenome.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside-resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|length | 1275
|mismatch | 234
|gapopen | 26
|qstart | 247
|qend | 1507
|sstart | 1855
|send | 596
|evalue | 0.0
|bitscore | 870
|genome | GUT_GENOME096550

## 7) Record example of `join_out.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside- resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|evalue | 0.0
|bitscore | 870
|arg_class | Aminoglycosides
|gen_lineage | d__Bacteria;p__Firmicutes_I;c__Bacilli_A;o__Paenibacillales;f__Paenibacillaceae;g__GM2;s__GM2 sp900069005
|genome | GUT_GENOME096550

## 8) Record example of `join_out_wPhylum.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside- resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|evalue | 0.0
|bitscore | 870
|arg_class | Aminoglycosides
|gen_lineage | d__Bacteria;p__Firmicutes_I;c__Bacilli_A;o__Paenibacillales;f__Paenibacillaceae;g__GM2;s__GM2 sp900069005
|genome | GUT_GENOME096550
|phylum | Firmicutes

## A) `join.py`
