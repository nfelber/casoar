# 1 Introduction

## 1.1 Antibiotics

Antibiotics (literally "against life") are antimicrobial substances widely used in medicine for antibacterial treatments. An antibiotic acts against a defined range of bacteria, either by killing them, or by inhibiting their growth.

Antibiotics came as a revolution when Alexander Fleming discovered penicillin in 1928, which is still widely used today. Accessibility of antibiotics in terms of price and availability encouraged an excessive use of them. Indeed, this revolution, which made it possible to cure previously deadly diseases and saved millions of lives, quickly appeared to have a major issue. Targeted bacteria can evolve to fight these compounds by developing antibiotic resistance mechanisms.

## 1.2 Antibiotic Resistance

With antibiotic resistance mechanisms spreading, antibiotics become less and less efficient. Fortunately, these resistance mechanisms come at a price for the bacteria, be it a need for energy, or a weakening in other defensive systems and vital functions. This implies that resistances will not appear without selective pressure from the environment, and will gradually disappear with time if selective pressure is not maintained and resistances are no longer advantageous.

What distinguishes antibiotics from other antimicrobials is that antibiotics are naturally produced in the environment by microorganisms not only as a means of defense, but also as a means of communication where they act as signaling molecules [REF](https://sci-hub.tw/https://doi.org/10.1021/cr2000509). Therefore, it is likely that antibiotics are ubiquitous to any habitat in which microbes co-exist, and thus, antibiotic resistance would be ubiquitous as well. In other words, we are particularly interested about antibiotics, because they encounter resistance through the natural adaptation of organisms and do not appear only as the result of human intervention.

Precisely, resistance mechanisms are naturally present in environments where antibiotics are found (produced by microorganisms). In such environments, the medical use of antibiotics will increase selective pressure, and induce greater prevalence of resistance determinants, which will make the antibiotics inefficient. Therefore, knowledge about the presence of specific antibiotic resistances in different environments is crucial to predict what impact a given antibiotic  will have when used in these environments.

## 1.3 The Human Gut Microbiota

We describe microbiota as the ecological communities of microorganisms living and interacting either inside or on multi-cellular organisms. The host organisms are then considered as the environments of these communities. A microbiome can either be described as the collection of all the genomes constituting the whole microbiota, or as the microorganisms themselves. We focused this study on the human gut microbiota, which hosts trillions of bacteria, and is considered to be playing a major role in metabolism, diseases, nutrition and even cognition [REF](https://www.ncbi.nlm.nih.gov/pubmed/24251697), in order to better predict the effects of antibiotics use on humans, and the resulting response of our microbiota.

## 1.4 Resistance Detection

Resistance mechanisms appear as the result of random genetic mutations that will be selected in case of pressure. Thus, we can detect them by analyzing the genetic information of the desired species. The genes responsible for resistance are called Antimicrobial Resistance Genes (ARGs).

Corresponding antimicrobials and mechanisms of a large number of resistance genes have already been studied and compiled into databases [REF]. We can determine that a species is likely to possess a given resistance if we can find the sequence of a matching resistance gene in its genome.

Likewise, bacterial genomes have been sequenced and compiled into more and more complete databases [REF] thanks to high throughput-squencing [REF](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3831009/), shotgun metagenomics methods [REF](https://www.nature.com/articles/nbt.3935) and progress in microbiological culture, giving everyone access to ever larger datasets to analyse. In our study, we focused on genomes sequenced from cultured bacteria. This had the effect of limiting the number and diversity of the organisms we studied, but the advantage of dealing with more complete and precise data, which is still likely representative of the whole microbiome.

In the second half of the 20th century, computer science has started bringing the power of algorithms [REF] and data analysis to modern days biology. With the more recent help of ever-increasing computing power and the availability of large databases [REF], we are now able to parse remarkably large amounts of data in little time, and thus explore a whole world of information we could not cope with otherwise. Our study took advantage of these new tools and possibilities to obtain results from the largest amount of data we could have at our disposal.

## 1.5 Hypothesis

For this project, we studied the human gastrointestinal microbiome, in order to determine what resistances were present. All the genetic information we used comes from cultured microorganisms, themselves coming from healthy adult human donors. Thus, we have good reasons to think that human use of antibiotics have not directly had a selective impact on these microbial populations.

Our hypothesis is that natural antibiotic resistance exists in the human gastrointestinal microbiota, and that we can find resistance determinants and their prevalence by searching for antibiotic resistance genes in healthy human gastrointestinal isolates.

## 1.6 Methods Summary

In order to test our hypothesis, we first found two databases [REF] containing annotated DNA sequences, one from 10,648 cultured human gastrointestinal microorganisms, and the other one from 7,868 antimicrobial resistance genes. Then we looked for the antimicrobial resistance genes in the human gastrointestinal microbial isolates, and listed every match. After that, we used these matches to determine which species were likely to be resistant and gathered them by phylum, for easier visualization. Then, we had to count the fraction of resistant species for each antimicrobial class and inside each phylum. In the end, we displayed the results in a two-dimensional heatmap, with the phyla and the antimicrobial classes as axes.

As previously stated, we limited ourselves to the distinction of bacteria by phylum and to the the study of genomes from cultured microorganisms. The limits of this project are further examined in the discussion section [REF].

# 2 Methods

## 2.1 Databases

For our project, we used the two following databases:

### 2.1.1 Unified Human Gastrointestinal Genome (UHGG)

The Unified Human Gastrointestinal Genome (UHGG) collection [REF](https://www.biorxiv.org/content/10.1101/762682v1.full) combines annotated genetic sequences from 286,997 genomes found in the human gut, 10,648 of which are isolates.

We downloaded the `genomes_metadata.tsv` file following [this link](http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/genomes_metadata.tsv). This `.tsv` file contains one record per genome, each of which contains information about the lineage, whether it is an isolate or a metagenome-assembled genomes (MAG) [REF](https://www.nature.com/articles/s41598-019-39576-6), which is culture-independant, an FTP link towards the DNA sequences and more.

#### Record example of `genomes_metadata.tsv`

| field            | value |
| :--------------- | :---- |
| Genome           | GUT_GENOME000001 |
| Original_name    | 11861_6_55 |
| Study_set        | HBC |
| Genome_type      | Isolate |
| Length           | 3221441 |
| N_contigs        | 137 |
| N50              | 47272 |
| GC_content       | 28.26 |
| Completeness     | 98.59 |
| Contamination    | 0.7 |
| rRNA_5S          | 88.24 |
| rRNA_16S         | 99.74 |
| rRNA_23S         | 99.83 |
| tRNAs            | 20 |
| Genome_accession | NA |
| Species_rep      | GUT_GENOME000001 |
| MGnify_accession | MGYG-HGUT-00001 |
| Lineage          | d__Bacteria;p__Firmicutes_A;c__Clostridia;|o__Peptostreptococcales;f__Peptostreptococcaceae;g__GCA-900066495;s__ |
| Sample_accession | ERS370061 |
| Study_accession  | ERP105624 |
| Country          | United Kingdom |
| Continent        | Europe |
| FTP_download     | ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/all_genomes/MGYG-HGUT-000/MGYG-HGUT-00001/genomes1/GUT_GENOME000001.gff.gz |

### 2.1.2 MEGARes: an Antimicrobial Database for High-Throughput Sequencing

[MEGARes](https://megares.meglab.org/) [REF](https://academic.oup.com/nar/article/48/D1/D561/5624973) is a database gathering DNA sequences from 7,868 antimicrobial resistance genes along with annotations.

We downloaded all the different files following [this link](https://megares.meglab.org/download/megares_v2.00.zip). We used two of the files found in this directory: `megares_modified_database_v2.00.fasta`, which contains the DNA sequences matched to a header, and `megares_modified_annotations_v2.00.csv`, which displays information about the classes, mechanisms and groups of the antimicrobial resistance genes.

#### Record example of `megares_modified_database_v2.00.fasta`

```
>Bla|OXA-223|JN248564|1-825|825|betalactams|Class_D_betalactamases|OXA
ATGAACATTAAAACACTCTTACTTATAACAAGCGCTATTTTTATTTCAGCCTGCTCACCTTATATAGTGACTGCTAATCCAAATCACAGCGCTTCAAAATCTGATGAAAAAGCAGAGAAAATTAAAAATTTATTTAACGAAGTACACACTACGGGTGTTTTAGTTATCCAACAAGGCCAAACTCAACAAAGCTATGGTAATGATCTTGCTCGTGCTTCGACCGAGTATGTACCTGCTTCGACCTTCAAAATGCTTAATGCTTTGATCGGCCTTGAGTACCATAAGGCAACCACCACAGAAGTATTTAAGTGGGACGGGCAAAAAAGGCTATTCCCAGAATGGGAAAAGGACATGACCCTAGGCGATGCTATGAAAGCTTCCGCTATTCCGGTTTATCAAGATTTAGCTCGTCGTATTGGACTTGAACTCATGTCTAAGGAAGTGAAGCGTGTTGGTTATGGCAATGCAGATATCGGTACCCAAGTCGATAATTTTTGGCTGGTGGGTCCTTTAAAAATTACTCCTCAGCAAGAGGCACAATTTGCTTACAAGCTAGCTAATAAAACGCTTCCCTTTAGCCCAAAAGTCCAAGATGAAGTGCAATCCATGCTATTCATAGAAGAAAAGAATGGAAATAAAATATACGCAAAAAGTGGTTGGGGATGGGATGTAAACCCACAAGTAGGCTGGTTAACTGGATGGGTTGTTCAGCCTCAAGGAAATATTGTAGCGTTCTCCCTTAACTTAGAAATGAAAAAAGGAATACCTAGCTCTGTTCGAAAAGAGATTACTTATAAAAGTTTAGAACAATTAGGTATTTTATAG
```
#### Record example of `megares_modified_annotations_v2.00.csv`

| field | value |
| :---- | :---- |
| header    | Bla\|OXA-223\|JN248564\|1-825\|825\|betalactams\|Class_D_betalactamases\|OXA |
| class     | betalactams |
| mechanism | Class D betalactamases |
| group     | OXA |

## 2.2 Preparing the Data from the Unified Human Gastrointestinal Genome Database

First, we extracted every record of the file `genomes_metadata.tsv` with the field `Genome_type` equal to `Isolate`. We created a new `isolates.tsv` file with the metadata of these 10,648 isolates. To do so, we used a program called [Miller](johnkerl.org/miller/doc/index.html).

```console
$ mlr --tsv filter '$Genome_type == "Isolate"' genomes_metadata.tsv > isolates.tsv
$ mlr --itsv --opprint count-distinct -f Genome_type isolates.tsv
count Genome_type
10648 Isolate
```

We then followed the FTP links under the field `FTP_download` of our `isolates.tsv` file and downloaded the corresponding `.gff.gz` file into a directory called `genomes` using `mlr` and `wget`. As these files are compressed, they need to be unzipped using `gunzip`. The total volume of the downloaded files is about 50 GB.

```console
$ mlr --itsv --onidx cut -f FTP_download < isolates.tsv | wget -P genomes -i -
$ gunzip -rv genomes
```

All the downloaded `.gff` files actually contain `.gff` and `.fasta` data delimited by a single line where a **##FASTA** tag stand.

#### Split example between `.gff` and `.fasta` in the files downloaded from UHGG

```
...
GUT_GENOME000001_125	barrnap:0.9	rRNA	360	461	6e-16	+	.	ID=GUT_GENOME000001_03274;locus_tag=GUT_GENOME000001_03274;product=5S ribosomal RNA
GUT_GENOME000001_127	Aragorn:1.2	tRNA	1	88	.	-	.	ID=GUT_GENOME000001_03275;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03275;product=tRNA-Leu(taa)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	124	197	.	-	.	ID=GUT_GENOME000001_03276;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03276;product=tRNA-Cys(gca)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	205	280	.	-	.	ID=GUT_GENOME000001_03277;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03277;product=tRNA-Lys(ttt)
GUT_GENOME000001_127	Aragorn:1.2	tRNA	289	365	.	-	.	ID=GUT_GENOME000001_03278;inference=COORDINATES:profile:Aragorn:1.2;locus_tag=GUT_GENOME000001_03278;product=tRNA-His(gtg)
##FASTA
>GUT_GENOME000001_1
CATAACATTTCTTTTCTTATAAATAAATTAATGGGTAAAAATGACTCTTTAGCTAAAATA
AAAGAACTAGTATTTGGATAAAATTTACAAATACTAGTCACTATGGGTCTATTTTTTTGC
AAAAAATATTTTACTTTATTTGGAAATTAAATTTAACTAGCACAATAGATTAATATATAT
AATTTTTTTATAACAGAGGAGATAGTAGTCTAGAAATAGCTTCTTTTATTTTGATAGATC
...
```

We could practically not work with these half-half files, so we had to split the files into a `.gff` and a `.fasta` version, using combinations of `sed` commands [REF].

```console
$ for filename in $(ls genomes); do
    sed -n '/##FASTA/,$p' < genomes/$filename \
    | sed '1d' \
    > $(echo genomes/$filename | sed 's/.gff/.fasta/');
done

$ for filename in $(ls genomes | sed -n '/.gff/p'); do
    sed -n '1,/##FASTA/p' < genomes/$filename \
    | sed '$d' \
    > genomes/$filename.tmp; \
    mv genomes/$filename.tmp genomes/$filename;
done
```

As we did not use the `.gff` data during the project, it is convenient to simply delete it from the files instead of splitting them in two. It is achieved by running this command instead of the two previous.

```console
$ for filename in $(ls genomes); do
    sed -i '1,/##FASTA/d' genomes/$filename;
done
```

## 2.3 BLAST : Basic Local Alignment Search Tool

[BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi) [REF] is a program that has become a standard in biology to find regions of similarity between biological sequences. We used it to find correlations between our UHGG genomes and the antimicrobial resistance genes of MEGARes, to detect the presence of antimicrobial resistance in these isolates.

In order to run, BLAST needs one query sequence as well as a database of sequences in which matches of the query sequence will be searched for. The database can then be used for many query sequences in turn, that can even be put together in a single file. The database is built from a single file containing all the genomes, that we called `all_genomes.fasta`.

```console
$ for filename in $(ls genomes | grep .fasta); do
    cat genomes/$filename \
    >> all_genomes.fasta;
done
```

This method will copy all of the `.fasta` data (about 45 GB), thus doubling the volume. To save space, we deleted the individual files as they were appended to `all_genomes.fasta`.

```console
$ for filename in $(ls genomes | grep .fasta); do
    cat genomes/$filename \
    >> all_genomes.fasta;
    rm genomes/$filename;
done
```

To make the BLAST database, we ran the following command.

```console
$ makeblastdb -in all_genomes.fasta -dbtype nucl
```

Finally, we performed a `blastn` (nucleotide BLAST) command to look for antimicrobial resistances from the MEGARes fasta data in the genomes of the UHGG database. We stored the output in `.tsv` format in a file called `blast_out.tsv`. Before running the command, we needed to prepare the output file with its headers, which can be found [here](http://www.metagenomics.wiki/tools/blast/blastn-output-format-6), as they are not automatically created by `blastn`.

```console
$ echo '1iqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore' > blast_out.tsv

$ blastn -query megares_v2.00/megares_modified_database_v2.00.fasta -db all_genomes.fasta -outfmt 6 >> blast_out.tsv
```

#### Record example of `blast_out.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside-resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|length | 1275
|mismatch | 234
|gapopen | 26
|qstart | 247
|qend | 1507
|sstart | 1855
|send | 596
|evalue | 0.0
|bitscore | 870

## 2.4 Join : Merge our Data Together

In data science, the *join* operation consists of merging two datasets together by combining records which contain the same value inside a specified field.

Here, each record of the `blast_out.tsv` file represents a region of similarity between a sequence from the query (an antimicrobial resistance gene) and a sequence from the database (a region of bacterial genome). Such matches between two regions are referred to as hits. They possess a field with header `1iqseqid` corresponding the `header` section of the file `megares_modified_annotations_v2.00.csv` and a field with header `sseqid` corresponding the `Genome` section of the file `genomes_metadata.tsv`, along with additional information about the score of similarity of the matched sequences.

However, the values of the `sseqid` section of `blast_out.tsv` do not exactly correspond to the values of the `Genome` section of `genomes_metadata.tsv`, because the former precises the DNA region where similarity with the ARG was found (GUT_GENOME096550 turns into GUT_GENOME096550_18). In order to join the two files, we must ensure the the two values are equal, and thus we created a new file called `blast_out_wGenome.tsv` with an additional field containing the name of the genome without the part indicating the DNA region.

```console
$ mlr --tsv put '$sseqid =~ "GUT_GENOME([0-9]+)_([0-9]+)"; $genome = "GUT_GENOME\1"' blast_out.tsv > blast_out_wGenome.tsv
```

#### Record example of `blast_out_wGenome.tsv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside-resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|length | 1275
|mismatch | 234
|gapopen | 26
|qstart | 247
|qend | 1507
|sstart | 1855
|send | 596
|evalue | 0.0
|bitscore | 870
|genome | GUT_GENOME096550

Then we merged `genomes_metadata.tsv` and `megares_annotations_v2.00.csv` using `blast_out_wGenome.tsv` to make the link between them. To do so, we used a custom join program written in c++ called `Join`. There is a convenient symbolic link to the executable (`src/join_cpp`) in our GitLab repository, along with a `Join` directory where all the source code can be found and a `README.md` file with instructions for use. The result is a `.csv` file called `join_out.csv`.

```console
$ join_cpp -i 'c0-b0 c1 c2 c10 c11 "arg_class"b2 "gen_lineage"a17 c12-a0' -o join_out.csv -d ',' isolates.tsv megares_v2.00/megares_modified_annotations_v2.00.csv blast_out_wGenome.tsv
```

#### Record example of `join_out.csv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside- resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|evalue | 0.0
|bitscore | 870
|arg_class | Aminoglycosides
|gen_lineage | d__Bacteria;p__Firmicutes_I;c__Bacilli_A;o__Paenibacillales;f__Paenibacillaceae;g__GM2;s__GM2 sp900069005
|genome | GUT_GENOME096550

In `join_out.csv`, each record corresponds to a hit of BLAST, and they contain one field for the antimicrobial class of the resistance gene, called `arg_class`, and one field for the lineage of the genome where the resistance was found, called `gen_lineage`. However, the latest contains the whole lineage, while we only need the name of the phylum. So we added a field called `phylum` to each record and filled it up with the name of the phylum extracted from the genome lineage. We stored the output in a file named `join_out_wPhylum.csv`.

```console
$ mlr --csv put '$gen_lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' join_out.csv > join_out_wPhylum.csv
```

#### Record example of `join_out_wPhylum.csv`

| field | value |
| :---- | :---- |
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside- resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNPConfirmation
|sseqid | GUT_GENOME096550_18
|pident | 79.373
|evalue | 0.0
|bitscore | 870
|arg_class | Aminoglycosides
|gen_lineage | d__Bacteria;p__Firmicutes_I;c__Bacilli_A;o__Paenibacillales;f__Paenibacillaceae;g__GM2;s__GM2 sp900069005
|genome | GUT_GENOME096550
|phylum | Firmicutes

## 2.5 Finding the Fraction of Resistant Species in each Phylum

After having successfully used the BLAST hits to link the phyla with the resistances found in their genomes in `join_out_wPhylum.csv`, we had to find the fraction of resistant species from each phylum and for each antibiotic class.

First, we appended a new `phylum` field to the file `isolates.tsv` where we indicated the phylum for each genome, that we extracted from the field `Lineage` of each record. We stored the new file as `isolates_wPhylum.tsv`.

```console
$ mlr --tsv put '$Lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' output/isolates.tsv > isolates_wPhylum.tsv
```

#### Record example of `isolates_wPhylum.tsv`

| field            | value |
| :--------------- | :---- |
| Genome           | GUT_GENOME000001 |
| Original_name    | 11861_6_55 |
| Study_set        | HBC |
| Genome_type      | Isolate |
| Length           | 3221441 |
| N_contigs        | 137 |
| N50              | 47272 |
| GC_content       | 28.26 |
| Completeness     | 98.59 |
| Contamination    | 0.7 |
| rRNA_5S          | 88.24 |
| rRNA_16S         | 99.74 |
| rRNA_23S         | 99.83 |
| tRNAs            | 20 |
| Genome_accession | NA |
| Species_rep      | GUT_GENOME000001 |
| MGnify_accession | MGYG-HGUT-00001 |
| Lineage          | d__Bacteria;p__Firmicutes_A;c__Clostridia;|o__Peptostreptococcales;f__Peptostreptococcaceae;g__GCA-900066495;s__ |
| Sample_accession | ERS370061 |
| Study_accession  | ERP105624 |
| Country          | United Kingdom |
| Continent        | Europe |
| FTP_download     | ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/2019_09/all_genomes/MGYG-HGUT-000/MGYG-HGUT-00001/genomes1/GUT_GENOME000001.gff.gz |
|phylum | Firmicutes |

Then we created a new `phyla` directory inside which we created one `.csv` file for each phylum found in `isolates_wPhylum.tsv`, wearing the name of that phylum (e.g. `Firmicutes.tsv`). Each of these files contains all the records of `join_out_wPhylum.csv` with the corresponding phylum. In other words, we divide the records of `join_out_wPhylum.csv` by phylum among these files.

```console
$ mkdir phyla

$ for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum isolates_wPhylum.tsv); do
    mlr --csv filter "\$phylum == \"$ph\"" join_out_wPhylum.csv \
    > phyla/$ph.csv;
done
```

Then we created a new directory for each phylum inside the directory `phyla`. Each of these directories is called according to the corresponding phylum and we populated them with a `.csv` file for each antibiotic class found in the file `join_out.csv`, i.e. all the antibiotic classes which have been found in at least one genome. Each of these files wears the name of the corresponding antibiotic class and contains the subset of records from `join_out_wPhylum.csv` that matches the corresponding phylum with the corresponding antibiotic class.

```console
$ for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum isolates_wPhylum.tsv); do
    mkdir phyla/$ph;
    for arg in $(mlr --icsv --onidx count-distinct -f arg_class then cut -f arg_class join_out.csv | sed 's/ /_/g'); do
        spacedarg=$(echo $arg | sed 's/_/ /g');
        mlr --csv filter "\$arg_class == \"$spacedarg\"" phyla/$ph.csv \
        > phyla/$ph/$arg.csv;
    done;
done
```

Once that we had sorted all the BLAST hits by phylum and by antibiotic class, we must determine how many species of each phylum are resistant to each antibiotic classes. To begin with, we did prepare the `.csv` file which would contain the final results in fraction of the total phylum. We called this file `arg_counts.csv` and started by writing its three headers.

```console
$ echo 'phylum,arg_class,counts' > arg_counts.csv
```

After that, we counted the number of unique genomes found in the BLAST hits for each combination of phylum and antibiotic class, and divided the obtained counts by the total number of genomes in the corresponding phylum (cf. appendix 1). We wrote each of these fractions to the file `arg_counts.csv`, along with the corresponding phylum and antibiotic class. We obtained the total number of genomes in each phylum by counting the distinct phyla in `isolates_wPhylum.tsv`.

```console
$ for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum isolates_wPhylum.tsv); do
    for arg in $(mlr --icsv --onidx count-distinct -f arg_class then cut -f arg_class join_out.csv | sed 's/ /_/g'); do
        echo "$ph,$arg,$(echo " \
            $(mlr --icsv --onidx count-distinct -f genome phyla/$ph/$arg.csv | wc -l) / \
            $(mlr --itsv --onidx count-distinct -f phylum then filter "\$phylum == \"$ph\"" then cut -f count isolates_wPhylum.tsv) \
        " \
        | bc -l)" \
        >> arg_counts.csv;
    done;
done
```

#### Record examples of `arg_counts.csv`

| phylum | arg_class | counts |
| :----- | :-------- | :----- |
| Firmicutes | Aminoglycoside | 0.48193244304791830322 |
| Firmicutes | Fosfomycin | 0.03613511390416339355 |
| Firmicutes | Drug_and_biocide_resistance | 0.22820109976433621366 |
| ... |

## 2.6 Displaying the results in a Heatmap using R

In order to visualize our results, we used the R programming language [REF](https://www.r-project.org/) to produce a two-dimensional heatmap which would display the fraction of species exhibiting antimicrobial resistance genes for every phylum and every antimicrobial class. Our Rscript can be found in our GitLab repository (`/src/heatmap3.r`). It takes the `.csv` file we want to use as a parameter.

```console
$ Rscript heatmap3.r arg_counts.csv
```

# 3 Results & Discussion

# 3.1 The Heatmap

`heatmap.png`

![heatmap.r output example](../ref_heatmap.png "heatmap.png")

Fig 1

The final results of our study are displayed in the heatmap of Fig 1. The different phyla we worked with, together with the name of the antimicrobial classes that were successfully matched, are respectively listed in the horizontal and vertical axes. We read in each cell the proportion of species from the corresponding phylum which are likely to be resistant to the corresponding antibiotic class. Each of these values are included in the range 0 to 1 and are color-coded according to the scale displayed on the top-left corner of the graphic.

The rows and columns of the heatmap are clustered and ordered according to the dendrograms (tree diagrams) which are displayed on the top and left sides of the heatmap. They indicate the distance computed between each row or column, in terms of similarity. Phyla with similar resistances distribution will appear close together in the tree, and hence the heatmap (for example, Euryarchaeota and Thermoplasmatota). Likewise, antibiotic classes wich are similarly distributed among the phyla will find themselves side to side in the tree and the heatmap (e.g. Aminoglycosides and Aminocoumarins). Note however that the opposite is not always true. A considerable difference can occur at the border of major branches, such as between β-lactams and Biguanide resistance, or between Thermoplasmatota and Proteobacteria for the phyla.

In fact, these dendrograms make groups appear. For the phyla, we observe two main branches: the first including the four left phyla, which exhibits especially strong resistance to Pactamycin and Phenicol but moderate resistance to the other compounds, and a second with the rest of the phyla, which in contrast exhibits almost no resistances to Pactamycin and Phenicol, but stronger resistance to Tetracyclines, MLS, Rifampin and Fluoroquinones, as well as mixed resistances in the upper rows of the heatmap.

As the phyla exhibit more or less close, yet very diverse patterns of resistance, we can conclude that the environment (which they have in common) is not the only factor responsible for resistance development.

In the other dimension, we can divide the classes of antimicrobials in four groups, from top to bottom: the first with the antimicrobials from Elfamycins to β-lactams, which are mainly found in the second branch of phyla, the second which includes antimicrobials from Biguanides to Iron with very few resistances found throughout the phyla, a third group with Fluoroquinones, Rifampin, MLS and Tetracyclines which encounters strong resistance in the second branch of the phyla, and finally a fourth branch with Pactamycin and Phenicol for which resistance strongly appears in the first group of the phyla.

Antimicrobials which encounter close to no resistance are likely not to be found naturally in the human gut environment in significant amounts or to be targeting too specific bacteria, else species would have adapted.

## 3.2 The special case of the Archaea

Halobacteria, Euryarchaeota and Thermoplasmatota are three phyla from the domain of the Archaea, which make them differ from all the other  microorganisms of our dataset which are bacteria. It is interesting to observe that these phyla are very close in the dendrogram of the heatmap and hence exhibit very similar resistances. Another interesting point is that Archaea are closer to animals, like human, than they are from actual bacteria. It implies that antibiotics toxic to Archaea might have higher risks of being toxic to us as well. Thus, it would be very difficult for us to find efficient treatments for Archaea. Fortunately, no known human pathogen belongs to Archaea. But it opens the question of why they seem to have developped at least some resistance to antibiotics that are known to be harmless to humans.

## 3.3 Trusting the results

Every conclusion that are made from results rely on the accuracy and the trustworthiness of said results. To evaluate the reliability of our results, we analyzed the elements of our methods can have an influence on their precision.

1) The databases: as our research starts with data from two databases which we did not compile ourselves, we have no choice but to trust the work of the authors who made them available. However, we have good reasons to think that these databases are trustworthy, because they are the result of serious and well documented studies [REF].

2) The algorithms: while the large majority of the algorithms we used, such as BLAST, were not from our production, they have already proven strong reliability in previous applications, and should not be a concern. However, a couple of the programs that have been used throughout the research were handcrafted, to explore and better understand the way they work and tailor them to our needs. As there is no evidence that these programs should work correctly, we decided to go through different testing procedures (cf. appendix B).

3) The results of BLAST: BLAST matches are hardly ever perfect. There are a couple values that help defining the accuracy of a match, one of which is the *expected value* (*evalue*). The *evalue* is calculated as being the number of similar matches that we could expect to find by pure chance, taking the size of the database into account. The lower this value, the better the match. Two DNA sequences are considered to match above a certain threshold for the *evalue*, which we kept at the default value of 10. An *evalue* of 10 means that we could expect 10 matches of similar characteristics to be found in the database. As 10 is a really poor score, we verified our worst hits and found that the highest *evalue* of the matches we found is 0.017 and that only the top 1% of our matches have *evalues* greater than 9.54e-10 (cf. appendix A), which means that 99% of our matches have less than one chance over a billion to have been found by chance. Thus, even if a couple false positives might have induced a bit of noise in our results, this noise is far from statistically significant.

## 3.4 Criticism of the results

The reliability of results of a statistical nature, such as the proportions of resistant species per phylum in our case, is very dependent on the number of observations out of which the statistics have been calculated. Our dataset did not contain the same number of genomes for each phylum. The precise numbers of genomes we treated for each phylum are displayed in appendix 1. The table of appendix 1 shows that we must be very careful with our results concerning certain phyla, such as the Halobacterota, Thermoplasmatota, Synergistota and Desulfobacterota from which we had only very limited data. Thus, it is likely that these few genomes are not representative of their whole phyla.

One major aspect of data science is that it is never possible to have an absolute point of view on the data, as soon as you extract information from it. Data scientists have to make a choice and decide what information to process and to show and what information to omit. Here will be discussed a couple choices we made that can be modified to result in other and likely complementary points of view.

1) The scope of the phylum: we decided to gather the 10,648 genomes that we analyzed by phylum. There are several reasons why we did this. It is convenient for the heatmap not to have too many columns and allows better visualization. It is also quite common to gather species by phylum as many studies have their focus on entire phyla and do not have reasons to enter into deeper subdivisions [REF?]. A last reason is that the distribution of our genomes is not homogeneous between the different phyla. With less than 10 genomes in four of the phyla (cf. appendix 1), it does not make sense to further subdivide. However, the drawback we encounter is that the distribution of antimicrobial resistances could be very uneven among species of a certain phylum, making the analysis by phylum irrelevant. Thus, repeating this study on specific phyla could be interesting, especially if we dispose of a lot of genomes in these. It is the case for the Firmicutes and the Proteobacteria, with respectively 2546 and 5845 different genomes each.

2) The resistance criteria: in our research, we considered a species to be resistant to antimicrobials if its genome contained at least one of these antimicrobial resistance genes, and non-resistant otherwise. Actually, as BLAST hits are not perfect, there is always the possibility for a gene that was counted as a match to be very similar to the antimicrobial resistance gene and however having a different function. It might perhaps act on a similar yet different chemical than the antimicrobial. Also, all the genes that are found in a genome are not necessarily expressed. It is possible for an antimicrobial resistance to appear without being ever used by the organism. There are two approaches to solving this problem. The first would be to experimentally test the individual species for the antibiotic resistances, which would be a very laborious work. The second would be to set a threshold for the number of occurrences of a given resistance which should appear to determine whether a species is resistance or not. For instance, only consider the species as being resistant if more than four occurences of the resistance gene are found in their genomes.

3) The resistance mechanisms: in our research, we totally omitted the mechanisms through which the microorganisms become resistant. It could be interesting to gather the antibiotic classes by resistance mechanisms to see the tendency of each phyla to invest in certain mechanisms more than others. It might for example answer the question of why resistances are not found in the same proportions among the different phyla, while they evolve in the same environment and under partly similar selective constraints. However, this is beyond the scope of this project. A starting point for mechanisms analysis is presented in appendix B.

## 3.5 Reproducibility

As it is crucial for any experiment to be reproducible in reasonable amounts of time and efforts, whenever possible, we wrote a shell script going through the whole process of our methods available to anyone who wants to reproduce our results. After cloning our GitLab repository [LINK], the script can be found at the address `src/pipeline2.sh`. It is necessary to run this script from the root of the git repository, with one argument indicating a name and location for the output directory which will be created at the beginning of the execution to store all the intermediates results (about 50 GB). This output directory can be deleted afterwards, as the resulting graphic will be created at the root of the git repository with the name `heatmap.pdf` along with a `.png` version, but it can be useful to keep it for anyone who wants to explore the intermediate files or slightly modify parts of the procedure.

All the programs used throughout our methods must be previously installed on the machine, with the exception of `join_cpp` and `heatmap3.r`, which we provide inside the GitLab repository.

An example of invocation of `pipeline2.sh`.

```console
$ bash src/pipeline2.sh ../output_dir
```

# 4 Conclusion

We proved that some human gut microorganisms exhibit antibiotic resistance genes by finding regions of significant similarity between genomes from cultured human gut microorganisms, found in the Unified Human Gastrointestinal Genome collection, and antibiotic resistance genes from the MEGARes collection as a result of BLAST.

In addition, we discovered that some antimicrobial resistance genes are much more present than others, as for instance in the case of the Tetracycline resistance. Our heatmap (Fig1) shows a number of resistance which are not found in significant amounts in any of the phyla we worked on. This gives us indications about the antimicrobials that are naturally found in the human gut microbiota.

Finally, we observed in our heatmap that the the different phyla do not exhibit the same distribution of antibiotic resistances, even if they are all found in the same environment, and subject to partly similar selective constraints. This implies that antibiotic resistances do not only depend on the environment, and that the phyla also have an importance.

# 5 Sources

[Wikipedia: Antibiotic](https://en.wikipedia.org/wiki/Antibiotic)
[Wikipedia: Microbiota](https://en.wikipedia.org/wiki/Microbiota)

# Appendices

## 1. Number of genomes from each phylum

|phylum | count |
| :---- | :---- |
|Firmicutes       | 2546
|Bacteroidota     | 791
|Proteobacteria   | 5845
|Actinobacteriota | 581
|Desulfobacterota | 9
|Fusobacteriota   | 45
|Campylobacterota | 755
|Synergistota     | 4
|Halobacterota    | 2
|Verrucomicrobiota| 37
|Euryarchaeota    | 26
|Thermoplasmatota | 7

## A) E-Values analysis

In order to analyse the accuracy of our BLAST matches, we sorted the file `blast_out.tsv` by decreasing order of evalue, and wrote the results to a file named `blast_out_decreasing_evalue.tsv`.

```console
$ mlr --tsv sort -nr evalue blast_out.tsv > blast_out_decreasing_evalue.tsv
```

Then, we looked at the first record of this file to determine what our highest evalue was equal to.

```console
$ mlr --itsv --onidx cut -f evalue blast_out_decreasing_evalue.tsv | head -n 1
0.017
```

Then we calculated the number of records that made the first 1% of `blast_out_decreasing_evalue.tsv`, by dividing the total number of records by 100.

```console
$ echo "($(wc -l blast_out_decreasing_evalue.tsv | sed -n 's/ .*//p') - 1) / 100" | bc   
14400
```

Finally, we looked at the evalue of the last record of the first 1% of `blast_out_decreasing_evalue.tsv`.

```console
$ head -n 14400 blast_out_decreasing_evalue.tsv | mlr --itsv --onidx cut -f evalue | tail -n 1
9.54e-10
```

## B) A starting point for mechanisms analysis

Anyone interested by the mechanisms responsible for resistance in our study and/or wants to further investigate in the topic might want to start with the two very simple files that are presented in this appendix.

Firstly, we can make a *join* between `join_out_wPhylum.csv` and `megares_modified_annotations_v2.00.csv` to get a new `mechanisms.csv` file which matches each BLAST hit with the resistance mechanism responsible for the corresponding resistance, with information about the antibiotic class and the phylum of the genome.

```console
$ join_cpp -o mechanisms.csv -i 'b2 b3 a8 a0-b0 a1' join_out_wPhylum.csv megares_v2.00/megares_modified_annotations_v2.00.csv
```

`mechanisms.csv` record example:

| field | value |
| :---- | :---- |
|class | Aminoglycosides
|mechanism | Aminoglycoside-resistant 16S ribosomal subunit protein
|phylum  |  Firmicutes
|1iqseqid | MEG_1\|Drugs\|Aminoglycosides\|Aminoglycoside-\|resistant_16S_ribosomal_subunit_protein\|A16S\|RequiresSNP
|sseqid  |  GUT_GENOME096550_18

Then, we can create a file `decreasing_mechanisms_counts.csv` which records the number of each unique resistance mechanism found in the BLAST hits, in decreasing order. We find that 190 different mechanisms are in use by the species of our dataset, in variable amounts.

```console
$ mlr --csv count-distinct -f mechanism then sort -nr count mechanisms.csv > decreasing_mechanisms_counts.csv

$ mlr --icsv --onidx cat decreasing_mechanisms_counts.csv | wc -l
190
```

`decreasing_mechanisms_counts.csv` records example:

|mechanism                                                |count
| :--- | :---
|Class A betalactamases                                   |465120
|Aminoglycoside O-nucleotidyltransferases                 |115464
|Class D betalactamases                                   |84793
|Class C betalactamases                                   |83829
|Tetracycline resistance ribosomal protection proteins    |63824
|Drug and biocide RND efflux pumps                        |49484
|Sulfonamide-resistant dihydropteroate synthases          |38553
|Aminoglycoside N-acetyltransferases                      |37464
|Dihydrofolate reductase                                  |31954
|...

These two files can be found in our GitLab repository [LINK] in the `appendices/` directory.

## C) Testing our programs

In order to test our programs and thus reduce the chances of involuntary mistakes that could affect the results of our project, we took two main measures.

The first one has been to create a program named `tester.py` which is found in the `test/` directory of our GitLab repository [LINK]. Its purpose is to apply any number of predefined tests to successive versions of a program as development progresses.

The first step is to create a `.txt` file with each line containing a shell command (namely, invocations of the program to test). These command must return text to the standard output, and their output should not change during development (they must already work as expected). For instance, they can test a specific part of a program that already works while the entire program is not yet working.

An example with the file `cmds.txt`, which here tests our `join.py` program.
```console
python3 ../src/join.py --pinline 'a0-b0 a2 a3 c0-b1 b2 c1 c2' test_data/data1.csv test_data/data2.csv test_data/data3.csv
python3 ../src/join.py --pinline 'a0 a1-d0 d1-e0 a2 a3 e1' test_data/data1.csv test_data/data2.csv test_data/data3.csv test_data/data4.csv test_data/data5.csv
```

Then, we run `tester.py` with the `create` argument, the name of the test file that is going to be created `test.txt` and the commands file `cmds.txt`.

```console
$ python3 tester.py create test.txt cmds.txt
```

The program creates a file called `test.txt` which contains the commands of `cmds.txt` followed by their output.

`test.txt`
```
>python3 ../src/join.py --pinline 'a0-b0 a2 a3 c0-b1 b2 c1 c2' test_data/data1.csv test_data/data2.csv test_data/data3.csv
Name,Age,Gender,ID,PRC,Hobby,Food
Amelie,25,Woman,357441,0.94,Tennis,Burger
Julien,22,Man,673853,0.85,Football,Steak
Beranger,45,Man,034819,0.99,Handball,Pineapple
Elisa,17,Woman,896754,0.74,Naps,Courgette
>python3 ../src/join.py --pinline 'a0 a1-d0 d1-e0 a2 a3 e1' test_data/data1.csv test_data/data2.csv test_data/data3.csv test_data/data4.csv test_data/data5.csv
Name,Surname,Number,Age,Gender,Color
Amelie,Nothomb,123,25,Woman,White
Beranger,Horn,943,45,Man,Yellow
Elisa,Bernardi,063,17,Woman,Black
Nathan,Felber,654,18,Man,Orange
Julien,Sorel,756,22,Man,Red
```

It is then possible to run `tester.py` with the `test` argument and the test file we want to use in order to verify if each command still returns the same output, which they normally should, except if something went wrong during development and made them dysfunction.

In this example, the second command passed the test successfully, while the first did not (here with the first letter deleted from its output, for illustrative purposes):

```console
$ python3 tester.py test test.txt
python3 ../src/join.py --pinline 'a0-b0 a2 a3 c0-b1 b2 c1 c2' test_data/data1.csv test_data/data2.csv test_data/data3.csv    -> error!
|       ---
|       +++
|       @@ -1,4 +1,4 @@
|       -ame,Age,Gender,ID,PRC,Hobby,Food
|       +Name,Age,Gender,ID,PRC,Hobby,Food
|        Amelie,25,Woman,357441,0.94,Tennis,Burger
|        Julien,22,Man,673853,0.85,Football,Steak
|        Beranger,45,Man,034819,0.99,Handball,Pineapple

python3 ../src/join.py --pinline 'a0 a1-d0 d1-e0 a2 a3 e1' test_data/data1.csv test_data/data2.csv test_data/data3.csv test_data/data4.csv test_data/data5.csv       -> ok!
```

This way, we could ensure our programs continued to work throughout the process of development.

The second measure we took was to rewrite the python version of our *join* program (which we made in the first place and was used above to illustrate our test procedures with `tester.py`) in c++. The c++ version of *join* is the one that has finally been used for the research, because of higher performances and extended features, but having two different versions of a same program that output the same results in the end is a reason to think that they work as expected.
