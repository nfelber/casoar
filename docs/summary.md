# Summary

## Abstract

The human gut microbiota is a complex environment where trillions of microorganisms interact with each other in an intricate manner.
This complexity makes it hard for scientists to predict the recation of the microbiota to any perturbation, such as the use of chemical compounds like antibiotics. More and more resistance to antibiotics was observed over the past decades, mostly due to an extensive usage in medicine and animal farming.
These resistances will make the targeted antibiotics inefficient when found in sufficient proportions in the environment where they are being used.

## Problem

The presence and the proportion of antibiotic resistances is hence a precious information to know prior to the prescription and administration of antibiotics. The goal of our research was to identify antibiotic resistances and measure their abundance in the human gut microbiota of healthy subjects in order to better predict the effect of different classes of antibiotics on human patients.

## Methodology

We compared genetic sequences of more than 7,500 known and annotated antimicrobial resistances with genomes of more than 10,000 cultured organisms from gastrointestinal microbiota of healthy human donors.
Each match indicates that the corresponding antimicrobial resistance is likely to be present in the corresponding species.
We then grouped the species by phyla in order to find the proportion of species resistant to the different antimicrobials among each phylum.
Finally, these proportions were displayed in a heatmap for visualization.

## Results

The heatmap shows that resistances are found among the microorganisms of the human gut microbiota. Some of them confer resistance to common antibiotics that are widely used, such as tetracyclines or fluoroquinolones.

However, it also shows that some antibiotic classes encounter close to no resistance at all among the tested species, like for metronidazole and lipopeptides.

Additionally, the resistances are quite heterogeneous between the different phyla for any given antimicrobial, which means that the environment, which they all have in common, is not the only factor for antibiotic resistances development.

## Conclusion

In conclusion, we have shown that antibiotic resistances are indeed naturally present in the human gut microbiota by finding regions of similarity between genomes from cultured human gut microorganisms and known antibiotic resistance genes.
We also noted that the distribution of resistances among the phyla, as well as the presence of resistances to the different antibiotic classes, is not homogeneous, meaning that antibiotic resistances are not a function of the environment alone and that the response to different antibiotics will probably differ in accordance with the class.
