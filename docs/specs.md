Casoar Project: Specs
=====================

Inputs
------

* Bacterial Genomes, incl. taxonomy, each genome as a Fasta file
* Antibiotic resistance genes (ARGs)
* Environment

### Sources

* ARGs: MEGARes
* Genomes: (cf. literature)

Outputs
-------

* A table of ARG hits in genomes, e.g.
  
```
|       | genome 1 | ... | genome m |
| ARG 1 | yes      | ... | no       |
| ...   | yes      | ... | no       |
| ARG n | no       | ... | yes      |

```

* Genomes have pointers to the corresponding phylum.

```csv
Genome,Phylum,ARG,BLAST Score
XBQ019,Firmicutes,β-lactamase,89
AZZ782,Actinobacteria,tetW,12
```

Finding Hits
------------

```console
$ python3 src/join_blast.py <megares_annotations_path> <genomes_metadata_path> <blast_results_path> [output_file_path]
```

References
----------

* [BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)
* [BioPython](https://biopython.org/)
