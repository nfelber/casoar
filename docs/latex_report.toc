\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Antibiotics}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Antibiotic Resistance}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Resistance Detection}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}The Human Gut Microbiota}{4}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Hypothesis}{5}{subsection.1.5}%
\contentsline {subsection}{\numberline {1.6}Methods Summary}{5}{subsection.1.6}%
\contentsline {section}{\numberline {2}Methods}{6}{section.2}%
\contentsline {subsection}{\numberline {2.1}Databases}{6}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Unified Human Gastrointestinal Genome (UHGG)}{6}{subsubsection.2.1.1}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {genomes\_metadata.tsv}}{6}{paragraph*.3}%
\contentsline {subsubsection}{\numberline {2.1.2}MEGARes: An Antimicrobial Database for High-Throughput Sequencing}{7}{subsubsection.2.1.2}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {megares\_modified\_annotations\_v2.00.csv}}{7}{paragraph*.5}%
\contentsline {subsection}{\numberline {2.2}Preparing the Data from the Unified Human Gastrointestinal Genome Database}{8}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}BLAST: Basic Local Alignment Search Tool}{9}{subsection.2.3}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {blast\_out.tsv}}{10}{paragraph*.7}%
\contentsline {subsection}{\numberline {2.4}Join: Merge our Data Together}{11}{subsection.2.4}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {blast\_out\_wGenome.tsv}}{11}{paragraph*.9}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {join\_out.csv}}{12}{paragraph*.11}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {join\_out\_wPhylum.csv}}{12}{paragraph*.13}%
\contentsline {subsection}{\numberline {2.5}Finding the Fraction of Resistant Species in each Phylum}{13}{subsection.2.5}%
\contentsline {paragraph}{\nonumberline Record example of \texttt {isolates\_wPhylum.tsv}}{13}{paragraph*.15}%
\contentsline {paragraph}{\nonumberline Record examples of \texttt {arg\_counts.csv}}{15}{paragraph*.17}%
\contentsline {subsection}{\numberline {2.6}Displaying the Results in a Heatmap using R}{16}{subsection.2.6}%
\contentsline {section}{\numberline {3}Results and Discussion}{16}{section.3}%
\contentsline {subsection}{\numberline {3.1}The Abundance of Antibiotic Resistances}{16}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Proportion of Species Resistant to a Class of Antibiotics}{17}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}The Distribution of Antibiotic Resistances}{19}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}The Special Case of the Archaea}{20}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Trusting the Results}{20}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Criticism of the Results}{21}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Reproducibility}{22}{subsection.3.7}%
\contentsline {section}{\numberline {4}Conclusion}{23}{section.4}%
\contentsline {section}{\numberline {5}Acknowledgements}{23}{section.5}%
\contentsline {section}{\numberline {6}References}{23}{section.6}%
\contentsline {section}{\numberline {A}Number of Genomes from Each Phylum}{}{appendix.A}%
\contentsline {section}{\numberline {B}E-Values Analysis}{}{appendix.B}%
\contentsline {section}{\numberline {C}A Starting Point for Mechanisms Analysis}{}{appendix.C}%
\contentsline {section}{\numberline {D}Testing our Programs}{}{appendix.D}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
