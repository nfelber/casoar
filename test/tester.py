import sys
import subprocess
import shlex
import difflib

# $ python3 tester.py create test.txt commands.txt
# $ python3 tester.py test test.txt

# commands.txt:
# python3 join.py --pinline 'a0-b0 a2 a3 c0-b1 b2 c1 c2' data1.csv data2.csv data3.csv
# python3 join.py --pinline 'a0 a1-d0 d1-e0 a2 a3 e1' data1.csv data2.csv data3.csv data4.csv data5.csv

# test.txt:
# >python3 join.py --pinline 'a0-b0 a2 a3 c0-b1 b2 c1 c2' data1.csv data2.csv data3.csv
# Name,Age,Gender,ID,PRC,Hobby,Food
# Amelie,25,Woman,357441,0.94,Tennis,Burger
# Julien,22,Man,673853,0.85,Football,Steak
# Beranger,45,Man,034819,0.99,Handball,Pineapple
# Elisa,17,Woman,896754,0.74,Naps,Courgette
# >python3 join.py --pinline 'a0 a1-d0 d1-e0 a2 a3 e1' data1.csv data2.csv data3.csv data4.csv data5.csv
# Name,Surname,Number,Age,Gender,Color
# Amelie,Nothomb,123,25,Woman,White
# Beranger,Horn,943,45,Man,Yellow
# Elisa,Bernardi,063,17,Woman,Black
# Nathan,Felber,654,18,Man,Orange
# Julien,Sorel,756,22,Man,Red


def query_arg(index, error_message="Couldn't find argument at specified index."):
    """Query an argv argument, printing an error message and exiting the program if the argument can't be found."""
    if len(sys.argv) < index + 1:
        print(error_message)
        sys.exit()
    return sys.argv[index]


mode = query_arg(1, "Mode not specified. Try 'test' or 'create'.")
test_file = query_arg(2, "Test file not specified.")

if mode == "create":
    cmd_file = query_arg(3, "Commands file not specified.")
    cmds = []

    # Filling up cmds with commands in cmd_file.
    with open(cmd_file, mode='r') as file:
        for line in file:
            cmds.append(line)

    with open(test_file, mode='w') as file:
        for cmd in cmds:
            # Executing the command and storing its output.
            process = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, universal_newlines=True)
            file.write(">" + cmd)  # Writing the command to test_file.
            file.write(process.stdout)  # And its output.

elif mode == "test":
    with open(test_file, mode='r') as file:
        cmd = ''
        cmd_to_out = {}
        for line in file:
            if line.startswith('>'):
                cmd = line[1:-1]
                cmd_to_out[cmd] = ''
            else:
                cmd_to_out[cmd] += line

    for cmd in cmd_to_out:
        process = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, universal_newlines=True)
        if process.stdout == cmd_to_out[cmd]:
            print("%s\t-> ok!" % cmd)
        else:
            print("%s\t-> error!" % cmd)
            for l in difflib.unified_diff(cmd_to_out[cmd].split('\n'), process.stdout.split('\n'), lineterm=''):
                print('|\t' + l)
            print()

else:
    print("Mode '%s' does not exist. Try 'test' or 'create'." % mode)
