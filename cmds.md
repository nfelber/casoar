sed 's/ /_/g' data/megares_v2.00/megares_modified_annotations_v2.00.csv > data/megares_v2.00/megares_modified_annotations_v2.00_no_spaces.csv

src/join_cpp -i 'b0 b1-a2 "arg_type"a1 b2' -o ../casoar-out/arg_counts_wTypes.csv data/megares_v2.00/megares_modified_annotations_v2.00_no_spaces.csv ../casoar-out/arg_counts.csv

for type in $(mlr --icsv --onidx uniq -f arg_type arg_counts_wTypes.csv); do mlr --csv filter "\$arg_type == \"$type\"" arg_counts_wTypes.csv > arg_counts_$type.csv; done
