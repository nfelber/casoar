Casoar TODO
===========

* [x] Download genomes
* [x] Download MEGARes ARGs
* [x] Install BLAST
* [x] Install BioPython
* [x] get familiar with launching BLASTN on 1 query ARG sequence on 1 genome (hint: use `makeblastdb` to prepare a BLAST db of the genome)
* [x] get familiar with launching BLASTN on a query DB on several genomes (but
  use a small subset for development)
