//
// Created by nf on 10.03.20.
//

#include <deque>
#include "JoinEngine.h"

class MaxSize {
public:
    bool operator()(std::string const &a, std::string const &b) {
        return a.size() < b.size();
    }
};

JoinEngine::JoinEngine(std::vector<CSVFile> filesIn, std::string fileOut, char outDelim) :
        m_filesIn(std::move(filesIn)), m_printOut(fileOut.empty()),
        m_fileOut(CSVFile(std::move(fileOut), false, outDelim)) {

    int a(97);
    for (auto & file : m_filesIn) {
        int i(0);
        for (auto & header : file.get_headers()) {
            m_headers_index[char(a) + std::to_string(i++)] = header;
        }
        ++a;
    }
}

void JoinEngine::printTable() const {
    std::vector<std::string> h;
    std::vector<int> sizes;
    for (auto &f : m_filesIn) {
        h = f.get_headers();
        h.push_back(f.get_path());
        auto it = std::max_element(h.begin(), h.end(), MaxSize());
        sizes.push_back(it->size() + 8);
        std::printf("%-*s", int(it->size()) + 8, f.get_path().c_str()); // -: left justify *: param for length s: string of chars
    }
    std::cout << std::endl;

    bool complete(false);
    int row(0);
    int col(0);
    while (!complete) {
        complete = true;
        int a(97);
        col = 0;
        for (auto &f : m_filesIn) {
            h = f.get_headers();
            if (h.size() > row) {
                std::printf("%-*s", sizes[col++], (char(a) + std::to_string(row) + ' ' + h[row]).c_str());
                complete = false;
            } else {
                std::printf("%*c", sizes[col++], ' ');
            }
            ++a;
        }
        std::cout << std::endl;
        ++row;
    }
}

void JoinEngine::performJoin(std::string const &instructions) {
    std::vector<std::string> instr_vector;
    std::stringstream ss(instructions);
    std::string token;
    while (std::getline(ss, token, ' ')) {
        instr_vector.push_back(token);
    }

    std::list<CSVData> intermediates;

    for (auto &instr : instr_vector) {
        int pos(instr.find('-'));
        if (pos != std::string::npos) { // string::npo -> not found at any position
            int quotesCount(0);
            std::pair<std::string, std::string> join;
            for (int i(0); i < pos; ++i) {
                if (instr[i] == '"') {
                    ++quotesCount;
                } else if (quotesCount % 2 == 0) {
                    join.first.push_back(instr[i]);
                }
            }
            for (int i(pos + 1); i < instr.size(); ++i) {
                join.second.push_back(instr[i]);
            }

            CSVData csv1;
            CSVData csv2;

            int p1(0);
            int p2(0);

            std::map<std::string, std::string> m1;
            std::map<std::string, std::string> m2;

            std::list<CSVData>::iterator it1;
            for (it1 = intermediates.begin(); it1 != intermediates.end(); ++it1) {
                std::vector<std::string> inter_headers(it1->get_headers());
                auto pos_it = std::find(inter_headers.begin(), inter_headers.end(), join.first);
                if (pos_it != inter_headers.end()) {
                    csv1 = *it1;
                    for (auto &h : inter_headers) {
                        m1[h] = h;
                    }
                    p1 = std::distance(inter_headers.begin(), pos_it);
                    break;
                }
            }

            std::list<CSVData>::iterator it2;
            for (it2 = intermediates.begin(); it2 != intermediates.end(); ++it2) {
                std::vector<std::string> inter_headers(it2->get_headers());
                auto pos_it = std::find(inter_headers.begin(), inter_headers.end(), join.second);
                if (pos_it != inter_headers.end()) {
                    csv2 = *it2;
                    for (auto &h : inter_headers) {
                        m2[h] = h;
                    }
                    p2 = std::distance(inter_headers.begin(), pos_it);
                    break;
                }
            }

            if (it1 != intermediates.end()) {
                intermediates.erase(it1);
            } else { // if csv1 has not been initialized
                csv1 = m_filesIn[int(join.first[0]) - 97];

                p1 = (std::stoi(join.first.substr(1)));

                std::vector<std::string> headers(csv1.get_headers());
                int i(0);
                for (auto & header : headers) {
                    m1[join.first[0] + std::to_string(i++)] = header;
                }
            }
            if (it2 != intermediates.end()) {
                if (it1 != it2) {
                    intermediates.erase(it2);
                }
            } else { // if csv2 has not been initialized
                csv2 = m_filesIn[int(join.second[0]) - 97];

                p2 = (std::stoi(join.second.substr(1)));

                std::vector<std::string> headers(csv2.get_headers());
                int i(0);
                for (auto & header : headers) {
                    m2[join.second[0] + std::to_string(i++)] = header;
                }
            }

            intermediates.push_back(csv1.joinWith(csv2, p1, p2, m1, m2));
        }
    }

    std::vector<std::string> headers_ordered;
    std::map<std::string, std::string> rename_map;

    for (auto & instr : instr_vector) {
        int end_pos(instr.find('-'));
        if (end_pos == std::string::npos) {
            end_pos = instr.size();
        }
        std::string index;
        for (int i(0); i<end_pos; ++i) {
            index.push_back(instr[i]);
        }

        int quote2_pos = index.rfind('"'); // last quote found

        if (quote2_pos == std::string::npos) {
            headers_ordered.push_back(index);
            rename_map[index] = m_headers_index.at(index);
        } else {
            headers_ordered.push_back(index.substr(quote2_pos+1, index.size()-1));
            rename_map[index.substr(quote2_pos+1, index.size()-1)] = index.substr(1, quote2_pos-1);
        }
    }

    m_fileOut.loadFromCSVData(intermediates.begin()->getSubCSVData(headers_ordered));
    m_fileOut.renameHeaders(rename_map);

    if (m_printOut) {
        m_fileOut.writeToStream(std::cout);
    } else {
        m_fileOut.writeToStream();
    }
}
