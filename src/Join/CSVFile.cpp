//
// Created by nf on 10.03.20.
//

#include "CSVFile.h"

CSVFile::CSVFile(std::string path, bool load, char delimiter) : CSVData(), m_path(std::move(path)) {
    if (delimiter != 0) {
        m_delimiter = delimiter;
    } else {
        m_delimiter = determineDelimiter();
    }

    if (load) {
        loadFromFile(m_path, m_delimiter);
    }
}

char CSVFile::determineDelimiter() const { // looks for a character repeated the same number of times on each line
    std::ifstream file(m_path);
    file.unsetf(std::ios_base::skipws); // keep invisible characters such as linebreaks

    std::istream_iterator<char> file_it(file);
    std::istream_iterator<char> end;

    std::map<char, int> charCount;
    std::map<char, int> line_charCount;
    std::map<char, int> tmp_charCount;

    int quotesCount(0);

    for (; *file_it != '\n' && file_it != end; ++file_it) {
        if (*file_it == '"') {
            ++quotesCount;
        }
        if (!quotesCount % 2 && *file_it != '\r') {
            ++charCount[*file_it];
        }
    }
    while (file_it != end) {
        ++file_it;
        quotesCount = 0;
        for (; *file_it != '\n' && file_it != end; ++file_it) {
            if (*file_it == '"') {
                ++quotesCount;
            }
            if (quotesCount%2 == 0 && charCount.count(*file_it) != 0) {
                ++line_charCount[*file_it];
            }
        }
        for (auto &i : line_charCount) {
            if (i.second == charCount[i.first]) {
                tmp_charCount[i.first] = i.second;
            }
        }
        charCount = tmp_charCount;
        if (charCount.size() <= 1) {
            break;
        }
        line_charCount.clear();
        tmp_charCount.clear();
    }

    if (charCount.size() == 1) {
        return charCount.begin()->first;
    }

    return 0;
}

char CSVFile::get_delimiter() const {
    return m_delimiter;
}

std::string CSVFile::get_path() const {
    return m_path;
}

void CSVFile::loadFromFile(std::string const &fileName, char delimiter) {
    std::ifstream file_stream(fileName);
    if (!file_stream.good()) {
        std::cerr << "Invalid file name: " << fileName << "." << std::endl;
        return;
    }
    std::string line;

    std::getline(file_stream, line);
    std::stringstream head_ss(line);
    std::string head_field;
    std::vector<std::string> headers;
    while (std::getline(head_ss, head_field, delimiter)) {
        headers.push_back(head_field);
    }
    set_headers(headers);

    while (std::getline(file_stream, line)) {
        std::vector<std::string> line_vector;
        std::stringstream line_ss(line);
        std::string field;
        while (std::getline(line_ss, field, delimiter)) {
            line_vector.push_back(field);
        }
        addRecord(line_vector);
    }
}

void CSVFile::writeToStream(std::ostream &stream) const {
    for (auto it(m_headers.begin()); it != m_headers.end(); ++it) {
        stream << *it;
        if (it + 1 == m_headers.end()) {
            stream << std::endl;
        } else {
            stream << m_delimiter;
        }
    }
    for (auto rec_it(m_records.begin()); rec_it != m_records.end(); ++rec_it) {
        for (auto it(rec_it->begin()); it != rec_it->end(); ++it) {
            stream << *it;
            if (it + 1 == rec_it->end()) {
                if (rec_it + 1 == m_records.end()) {
                    return;
                }
                stream << std::endl;
            } else {
                stream << m_delimiter;
            }
        }
    }
}

void CSVFile::writeToStream() const {
    std::ofstream ofs(m_path);
    writeToStream(ofs);
}

void CSVFile::loadFromCSVData(CSVData const &csvData) {
    m_headers = csvData.get_headers();
    m_records = csvData.get_records();
}

void CSVFile::set_delimiter(char d) {
    m_delimiter = d;
}
