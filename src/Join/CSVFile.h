//
// Created by nf on 10.03.20.
//

#ifndef JOIN_CSVFILE_H
#define JOIN_CSVFILE_H

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>
#include <vector>
#include <sstream>

#include "CSVData.h"

class CSVFile : public CSVData {

private:
    std::string m_path;
    char m_delimiter;

    char determineDelimiter() const;

public:
    explicit CSVFile(std::string path, bool load=false, char delimiter=0);

    void loadFromCSVData(CSVData const &csvData);
    void loadFromFile(std::string const &fileName, char delimiter);
    void writeToStream(std::ostream &stream) const;
    void writeToStream() const;

    void set_delimiter(char d);
    char get_delimiter() const;
    std::string get_path() const;
};


#endif //JOIN_CSVFILE_H
