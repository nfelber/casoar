//
// Created by nf on 10.03.20.
//

#ifndef JOIN_JOINENGINE_H
#define JOIN_JOINENGINE_H

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include "CSVFile.h"

class JoinEngine {

private:
    bool m_printOut;
    CSVFile m_fileOut;
    std::vector<CSVFile> m_filesIn;
    std::map<std::string, std::string> m_headers_index;

public:
    explicit JoinEngine(std::vector<CSVFile> filesIn, std::string fileOut = "", char outDelim = ',');
    void printTable() const;
    void performJoin(std::string const &instructions);
};


#endif //JOIN_JOINENGINE_H
