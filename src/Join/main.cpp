#include <iostream>
#include "CSVFile.h"
#include "JoinEngine.h"

int main(int argc, char** argv) {

    std::string fileOut;
    std::string instructions;
    std::vector<CSVFile> files;
    char outDelim(',');

    for (int i(1); i<argc; ++i) {
        if (std::string(argv[i]) == "--out" || std::string(argv[i]) == "-o") {
            if (i + 1 < argc) {
                fileOut = argv[++i];
            }
        } else if (std::string(argv[i]) == "--instructions" || std::string(argv[i]) == "-i") {
            if (i + 1 < argc) {
                instructions = argv[++i];
            }
        } else if (std::string(argv[i]) == "--outdelim" || std::string(argv[i]) == "-d") {
            if (i+1 < argc) {
                outDelim = *argv[++i];
            }
        } else {
            files.emplace_back(CSVFile(argv[i], true));
        }
    }

    JoinEngine join(files, fileOut, outDelim);

    if (instructions.empty()) {
        join.printTable();
        std::cout << std::endl << "Instructions: ";
        std::istream is(std::cin.rdbuf());
        std::getline(is, instructions);
    }

    join.performJoin(instructions);

    return 0;
}
