//
// Created by nf on 23.03.20.
//

#include "CSVData.h"

#include <utility>

CSVData::CSVData() = default;

std::vector<std::string> CSVData::get_headers() const {
    return m_headers;
}

CSVData CSVData::joinWith(CSVData const &other, int column, int otherColumn,
        std::map<std::string, std::string> const &header_map,
        std::map<std::string, std::string> const &other_header_map) const {

    CSVData output;

    std::vector<std::string> headers;
    headers.reserve(header_map.size() + other_header_map.size());
    for (auto & pair : header_map) {
        headers.push_back(pair.first);
    }
    for (auto & pair : other_header_map) {
        headers.push_back(pair.first);
    }

    output.set_headers(headers);

    for (auto & record : m_records) {
        std::vector<std::string> other_record(other.findRecord(record[column], otherColumn));
        if (!other_record.empty()) {
            std::vector<std::string> output_record;

            for (auto & header : headers) {
                if (header_map.count(header) != 0) {
                    auto it = std::find(m_headers.begin(), m_headers.end(), header_map.at(header));
                    output_record.push_back(record[std::distance(m_headers.begin(), it)]);
                } else {
                    std::vector<std::string> other_headers(other.get_headers());
                    auto it = std::find(other_headers.begin(), other_headers.end(),
                            other_header_map.at(header));
                    output_record.push_back(other_record[std::distance(other_headers.begin(), it)]);
                }
            }

            output.addRecord(output_record);
        }
    }

    return output;
}

std::vector<std::string> CSVData::findRecord(std::string const &value, int column) const {
    for (auto & record : m_records) {
        if (record[column] == value) {
            return record;
        }
    }
    return std::vector<std::string>();
}

void CSVData::set_headers(std::vector<std::string> headers) {
    m_headers = std::move(headers);
}

void CSVData::addRecord(std::vector<std::string> const &record) {
    m_records.push_back(record);
}

CSVData CSVData::getSubCSVData(std::vector<std::string> const &headers) const {
    CSVData output;

    output.set_headers(headers);

    for (auto & record : m_records) {
        std::vector<std::string> output_record;
        for (auto & header : headers) {
            auto pos_it = std::find(m_headers.begin(), m_headers.end(), header);
            output_record.push_back(record[std::distance(m_headers.begin(), pos_it)]);
        }
        output.addRecord(output_record);
    }

    return output;
}

void CSVData::renameHeaders(std::map<std::string, std::string> const &rename_map) {
    for (auto it(m_headers.begin()); it != m_headers.end(); ++it) {
        if (rename_map.count(*it) != 0) {
            *it = rename_map.at(*it);
        }
    }
}

std::vector<std::vector<std::string>> CSVData::get_records() const {
    return m_records;
}
