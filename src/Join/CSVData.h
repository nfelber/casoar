//
// Created by nf on 23.03.20.
//

#ifndef JOIN_CSVDATA_H
#define JOIN_CSVDATA_H

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>

class CSVData {
protected:
    std::vector<std::string> m_headers;
    std::vector<std::vector<std::string>> m_records;

public:
    explicit CSVData();

    std::vector<std::string> get_headers() const;
    void set_headers(std::vector<std::string> headers);
    std::vector<std::vector<std::string>> get_records() const;

    std::vector<std::string> findRecord(const std::string& value, int column) const;
    void addRecord(std::vector<std::string> const &record);

    CSVData getSubCSVData(std::vector<std::string> const &headers) const;

    void renameHeaders(std::map<std::string, std::string> const &rename_map);

    CSVData joinWith(CSVData const &other, int column, int otherColumn,
                     std::map<std::string, std::string> const &header_map,
                     std::map<std::string, std::string> const &other_header_map) const;

};


#endif //JOIN_CSVDATA_H
