import sys
import csv
import string


def get_delimiter(path):
	delimiter = ','
	if path[-3] == 't':
		delimiter = '\t'
	return delimiter


abc = string.ascii_letters

inline = False
print_out = False
instr = ""
out_file = ""
file_paths = sys.argv[1:]

if sys.argv[1] == "--inline":
	inline = True
	instr = sys.argv[2]
	out_file = sys.argv[3]
	file_paths = sys.argv[4:]
elif sys.argv[1] == "--pinline":
	inline = True
	print_out = True
	instr = sys.argv[2]
	file_paths = sys.argv[3:]

display_table = []
column_max_widths = []

final_fields = []
id_to_field = {}
file_ids_to_final_fields = {}
links = {}

for i_file, path in enumerate(file_paths):
	file_name = path.split('/')[-1]
	column_max_widths.append(len(file_name))
	
	display_table.append(0)
	
	links[abc[i_file]] = {}
	file_ids_to_final_fields[abc[i_file]] = {}

	with open(path, mode='r') as csv_file:
		csv_reader = csv.DictReader(csv_file, delimiter=get_delimiter(path))
		for i_fn, fn in enumerate(csv_reader.fieldnames):
			id_to_field[abc[i_file] + str(i_fn)] = fn
			display_table[i_file] += 1
			
			if len(str(i_fn) + fn)+2 > column_max_widths[i_file]:
				column_max_widths[i_file] = len(str(i_fn) + fn)+2

if not inline:
	print("Fieldnames for input files:\n")
	for i, path in enumerate(file_paths):
		print("{:<{width}}".format(path.split('/')[-1], width=column_max_widths[i]+5), end='')
	print('\n')

	for i_row in range(max(display_table)):
		for i_col, col in enumerate(display_table):
			if col - i_row > 0:
				field_id = abc[i_col] + str(i_row)
				print("{:<{width}}".format(field_id + ' ' + id_to_field[field_id], width=column_max_widths[i_col]+5), end='')
			else:
				print("{:<{width}}".format('', width=column_max_widths[i_col]+5), end='')
		print()

	instr = input("\nJoin instructions: ")

if instr == 'q':
	sys.exit()

instr_list = instr.split(' ')

for f in instr_list:
	field = f # str are not copied by reference
	str_field = ""
	if field[0] == '"':
		spl = field.split('"')
		str_field += '=' + spl[1]
		field = spl[2]
	if '-' in field:
		spl = field.split('-')
		links[spl[0][0]][spl[0]] = spl[1]
		links[spl[1][0]][spl[1]] = spl[0]
		file_ids_to_final_fields[spl[1][0]][spl[1]] = id_to_field[spl[1]] + '=' + id_to_field[spl[0]] + str_field
		field = spl[0]
		
	str_field = id_to_field[field] + str_field
	file_ids_to_final_fields[field[0]][field] = str_field
	final_fields.append(str_field.split('=')[-1])

final_table = []

def r_join(links_dict):
	global final_table
	while len(links_dict) != 0:
		k, v = links_dict.popitem()
		links[v[0]].pop(v)
		path = file_paths[abc.index(v[0])]
		final_table_tmp = []
		with open(path, mode='r') as csv_file:
			csv_reader = csv.DictReader(csv_file, delimiter=get_delimiter(path))
			for row2 in csv_reader:
				for row1 in final_table:
					if row1[file_ids_to_final_fields[k[0]][k].split('=')[-1]] == row2[id_to_field[v]]:
						d = {}
						for field in final_fields:
							d[field] = row1[field]
							for f in file_ids_to_final_fields[v[0]].values():
								if f.split('=')[-1] == field:
									d[field] = row2[f.split('=')[0]]
									break
						final_table_tmp.append(d)
		final_table = final_table_tmp
		r_join(links[v[0]])

for file_id in links:
	for field_id in file_id:
		if len(final_table) == 0:
			path = file_paths[abc.index(file_id)]
			with open(path, mode='r') as csv_file:
				csv_reader = csv.DictReader(csv_file, delimiter=get_delimiter(path))
				for row in csv_reader:
					d = {}
					for field in final_fields:
						d[field] = '-'
						for f in file_ids_to_final_fields[file_id].values():
							if f.split('=')[-1] == field:
								d[field] = row[f.split('=')[0]]
								break
					final_table.append(d)
			r_join(links[file_id])

if not inline:
	out_file = input("Output file's name: ")
if not '.' in out_file:
	out_file += '.csv'

if print_out:
	print(','.join(final_fields))
	for d in final_table:
		print(','.join(d.values()))
else:
	with open(out_file, "w", newline="") as csv_file:
		writer = csv.DictWriter(csv_file, delimiter=get_delimiter(out_file), fieldnames=final_fields)

		writer.writeheader()
		for d in final_table:
			writer.writerow(d)
