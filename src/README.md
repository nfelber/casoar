# PROGRAMS USAGE:

## join.py

```
$ python3 join.py [options] [files]
```

Merge the data of different `.csv` or `.tsv` files with common values under specific fields into one single output.

### Options

With no options, `join.py` prints the headers of the [files] associated to unique alphanumeric codes, then asks for an instruction command (see Instructions syntax).

`--inline {instructions}`
Join instructions. Does not print the file headers, but still ask for the name output file during execution.

`--pinline {instructions}`
Same as --inline, except that it does not ask for the name of an output file and prints the result to the standard output.

### Files

`.csv` and `.tsv` files supported. Up to 26 at once.

### Instructions syntax

Each expression defines a header of the `.csv` or `.tsv` output file. They are separated by spaces. An expression refers to the headers (and hence the columns) of the input files by the associated alphanumeric code. The files are associated to letters in the alphabetical order, and their header are numbered from 0, in respect to their order.

Example of expressions:

| Expression | Meaning
| :--------- | :-----
|a0 | Append the first column of the first file to the output.
|b2-a1 | Join the third column of the second file and the second column of the first file, then append it to the output with the name of the third header of the second file.
|"New_name"a3 | Append the fourth column of the first file to the output, and rename its header to *New_name*.

### Examples

```console
$ python3 join.py ../test/test_data/*          
Fieldnames for input files:

data1.csv      data2.csv     data3.csv     data4.csv         data5.csv     

a0 Name        b0 ID1        c0 ID         d0 Identifier     e0 No         
a1 Surname     b1 ID2        c1 Hobby      d1 Number         e1 Color      
a2 Age         b2 PRC        c2 Food                                       
a3 Gender                    c3 Mother                                     
a4 Animal                    c4 Height                                     

Join instructions: a0-b0 a1 c1 "Precision"b2 "Identifier"b1-c0
Output file's name: out.csv
```

`out.csv`
```
Name,Surname,Hobby,Precision,Identifier
Amelie,Nothomb,Tennis,0.94,357441
Julien,Sorel,Football,0.85,673853
Beranger,Horn,Handball,0.99,034819
Elisa,Bernardi,Naps,0.74,896754
```

With `--pinline`:

```console
$ python3 join.py --pinline 'a0 a1-d0 "Colour"e1 d1-e0' ../test/test_data/*
Name,Surname,Colour,Number
Amelie,Nothomb,White,123
Beranger,Horn,Yellow,943
Elisa,Bernardi,Black,063
Nathan,Felber,Orange,654
Julien,Sorel,Red,756
```

## join_cpp

```
$ join_cpp [options] [files]
```

Merge the data of different `.csv`-like files with common values under specific fields into one single output.

### Options

With no options, `join_cpp` prints the headers of the [files] associated to unique alphanumeric codes, then asks for an instruction command (see Instructions syntax).

`-o --out {file}`
Output file. Default: prints to standard output

`-d --outdelim {delimiter}`
Delimiter to use for the output file. Default: `,`.

`-i --instructions {instructions}`
Join instructions. Does not print the file headers.

### Files

Any `.csv`-like file supported. Up to 26 at once.

### Instructions syntax

Each expression defines a header of the output file. They are separated by spaces. An expression refers to the headers (and hence the columns) of the input files by the associated alphanumeric code. The files are associated to letters in the alphabetical order, and their header are numbered from 0, in respect to their order.

Example of expressions:

| Expression | Meaning
| :--------- | :-----
|a0 | Append the first column of the first file to the output.
|b2-a1 | Join the third column of the second file and the second column of the first file, then append it to the output with the name of the third header of the second file.
|"New_name"a3 | Append the fourth column of the first file to the output, and rename its header to *New_name*.

### Examples

```console
$ ./join_cpp -o out.csv ../test/test_data/*
../test/test_data/data1.csv        ../test/test_data/data2.csv        ../test/test_data/data3.csv        ../test/test_data/data4.csv        ../test/test_data/data5.csv        
a0 Name                            b0 ID1                             c0 ID                              d0 Identifier                      e0 No                              
a1 Surname                         b1 ID2                             c1 Hobby                           d1 Number                          e1 Color                           
a2 Age                             b2 PRC                             c2 Food                                                                                                  
a3 Gender                                                             c3 Mother                                                                                                
a4 Animal                                                             c4 Height                                                                                                


Instructions: a0-b0 a1 c1 "Precision"b2 "Identifier"b1-c0
```

`out.csv`
```
Name,Surname,Hobby,Precision,Identifier
Julien,Sorel,Football,0.85,673853
Beranger,Horn,Handball,0.99,034819
Amelie,Nothomb,Tennis,0.94,357441
Elisa,Bernardi,Naps,0.74,896754
```

Inline instructions and standard output:

```console
$ ./join_cpp -i 'a0 a1-d0 "Colour"e1 d1-e0' -d "$(echo '\t')" ../test/test_data/*
Name     Surname  Colour  Number
Julien   Sorel    Red     756
Beranger Horn     Yellow  943
Amelie   Nothomb  White   123
Nathan   Felber   Orange  654
Elisa    Bernardi Black   063
```
