#!/bin/bash

SRC_DIR=./src
DB_DIR=./data
OUT_DIR=$1

# MAKE OUT_DIR
mkdir $OUT_DIR

# FILTERING ISOLATES
mlr --tsv filter '$Genome_type == "Isolate"' $DB_DIR/genomes_metadata.tsv > $OUT_DIR/isolates.tsv

# DOWNLOAD GENOMIC DATA
mlr --itsv --onidx cut -f FTP_download < $OUT_DIR/isolates.tsv | wget -P $OUT_DIR/genomes -i -
gunzip -rv $OUT_DIR/genomes

# GETTING FASTA DATA AND APPENDING IT TO ONE SINGLE FILE
for f in $(ls $OUT_DIR/genomes); do
    sed -n '/##FASTA/,$p' < $OUT_DIR/genomes/$f \
    | sed '1d' \
    >> $OUT_DIR/all_genomes.fasta;
    rm $OUT_DIR/genomes/$f;
done

rmdir $OUT_DIR/genomes

# BLAST DATABASE
makeblastdb -in $OUT_DIR/all_genomes.fasta -dbtype nucl

# HEADERS
echo '1iqseqid	sseqid	pident	length	mismatch	gapopen	qstart	qend	sstart	send	evalue	bitscore' > $OUT_DIR/blast_out.tsv

# BLASTN
blastn -query $DB_DIR/megares_v2.00/megares_modified_database_v2.00.fasta -db $OUT_DIR/all_genomes.fasta -outfmt 6 >> $OUT_DIR/blast_out.tsv

# PREPARE FOR JOIN
mlr --tsv put '$sseqid =~ "GUT_GENOME([0-9]+)_([0-9]+)"; $genome = "GUT_GENOME\1"' $OUT_DIR/blast_out.tsv > $OUT_DIR/blast_out_wGenome.tsv

# JOIN
$SRC_DIR/join_cpp -i 'c0-b0 c1 c2 c10 c11 "arg_class"b2 "gen_lineage"a18 c12-a0' -o $OUT_DIR/join_out.csv -d ',' $OUT_DIR/isolates.tsv $DB_DIR/megares_v2.00/megares_modified_annotations_v2.00.csv $OUT_DIR/blast_out_wGenome.tsv

# EXTRACT PHYLUM
mlr --csv put '$gen_lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' $OUT_DIR/join_out.csv > $OUT_DIR/join_out_wPhylum.csv

# ADD PHYLA TO RECORDS OF ISOLATES
mlr --tsv put '$Lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' $OUT_DIR/isolates.tsv > $OUT_DIR/isolates_wPhylum.tsv

# CREATE PHYLA DIRECTORY
mkdir $OUT_DIR/phyla

# DIVIDE ISOLATES BY PHYLUM
for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum $OUT_DIR/isolates_wPhylum.tsv); do
    mlr --csv filter "\$phylum == \"$ph\"" $OUT_DIR/join_out_wPhylum.csv \
    > $OUT_DIR/phyla/$ph.csv;
done

# FURTHER DIVIDE ISOLATES BY ARG
for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum $OUT_DIR/isolates_wPhylum.tsv); do
    mkdir $OUT_DIR/phyla/$ph;
    for arg in $(mlr --icsv --onidx count-distinct -f arg_class then cut -f arg_class $OUT_DIR/join_out.csv | sed 's/ /_/g'); do
        spacedarg=$(echo $arg | sed 's/_/ /g');
        mlr --csv filter "\$arg_class == \"$spacedarg\"" $OUT_DIR/phyla/$ph.csv \
        > $OUT_DIR/phyla/$ph/$arg.csv;
    done;
done

# PREPARE ARG_COUNTS
echo 'phylum,arg_class,counts' > $OUT_DIR/arg_counts.csv

for ph in $(mlr --itsv --onidx count-distinct -f phylum then cut -f phylum $OUT_DIR/isolates_wPhylum.tsv); do
    for arg in $(mlr --icsv --onidx count-distinct -f arg_class then cut -f arg_class $OUT_DIR/join_out.csv | sed 's/ /_/g'); do
        echo "$ph,$arg,$(echo "\
            $(mlr --icsv --onidx count-distinct -f genome $OUT_DIR/phyla/$ph/$arg.csv | wc -l) /\
            $(mlr --itsv --onidx count-distinct -f phylum then filter "\$phylum == \"$ph\"" then cut -f count $OUT_DIR/isolates_wPhylum.tsv)" \
        | bc -l)" \
        >> $OUT_DIR/arg_counts.csv;
    done;
done

# HEATMAP
Rscript $SRC_DIR/heatmap3.r $OUT_DIR/arg_counts.csv

# CONVERT TO PNG
convert -density 150 heatmap.pdf -quality 90 heatmap.png
