library(gplots)
library(tidyr)
library(viridis)

args = commandArgs(trailingOnly=TRUE)

outfile <- "heatmap.pdf"

# test if there is at least one argument: if not, return an error
if (length(args) < 1) {
  stop("Usage: heatmap.r <input-file> [output-file]", call.=FALSE)
} else if (length(args) > 1) {
	outfile <- args[2]
}

infile <- args[1]

#infile <- "~/casoar-out/arg_counts.csv"

data <- subset(read.csv(infile), select = c("phylum","arg_class", "counts"))

spd <- spread(data, phylum, counts)

data_matrix <- as.matrix(spd[2:ncol(spd)])

rownames(data_matrix) <- spd$arg_class

pdf(outfile, width=16, height=12)
heatmap.2(data_matrix,
          col=viridis(256),
          key=TRUE,
          key.title = NA,
          key.ylab = NA,
          key.xlab = "relative frequency",
          symbreaks = FALSE,
          density.info="none", # "histogram"
          trace="none", # "column"
          srtCol = 60,
          cexRow = 0.8,
          colRow = c(5, 2, 4),
          margins = c(10, 20))
dev.off()

