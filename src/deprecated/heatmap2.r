library(viridis)
library(d3heatmap)
library(heatmaply)
library(gplots)
library(tidyr)

args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (1 != length(args)) {
  stop("Usage: hitmap.r <input-file>", call.=FALSE)
}

file <- args[1]

# file <- "~/maturity_project/output/join_out_wPhylum.tsv"

data <- read.delim(file)

data_table <- table(data$arg_class, data$phylum)

normalized_data_table <- data_table / colSums(data_table)[col(data_table)]

pdf("heatmap.pdf", width=16, height=12)
heatmap.2(normalized_data_table,
          col=viridis(256),
          key=TRUE,
          key.title = NA,
          key.ylab = NA,
          key.xlab = "relative frequency",
          symbreaks = FALSE,
          density.info="none", # "histogram"
          trace="none", # "column"
          srtCol = 60,
          margins = c(10, 20))
dev.off()

