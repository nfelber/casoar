import sys
import csv

class Blast_result:
	def __init__(self, qseqid, sseqid, pident, evalue, bitscore, arg_classes, gen_phylums):
		self.qseqid = qseqid
		self.sseqid = sseqid
		self.pident = pident
		self.evalue = evalue
		self.bitscore = bitscore
		
		self.arg_class = arg_classes[qseqid]
		self.gen_phylum = gen_phylums["_".join(sseqid.split("_")[:-1])]

megares_annotations_path = sys.argv[1]
genomes_metadata_path = sys.argv[2]
blast_results_path = sys.argv[3]

output_path = "output.tsv"
if len(sys.argv) > 4:
    output_path = sys.argv[4]

#try:
#	output_path = sys.argv[4]
#except:
#	output_path = "output.tsv"

arg_classes = {}
with open(megares_annotations_path, mode="r") as megares_annotations_csv:
	csv_reader = csv.DictReader(megares_annotations_csv, delimiter=",")
	for row in csv_reader:
		arg_classes[row["header"]] = row["class"]

gen_phylums = {}
with open(genomes_metadata_path, mode="r") as genomes_metadata_tsv:
	tsv_reader = csv.DictReader(genomes_metadata_tsv, delimiter="\t")
	for row in tsv_reader:
		gen_phylums[row["Genome"]] = row["Lineage"].split(";")[1].split("_")[2]

blast_results = []
with open(blast_results_path, mode="r") as blast_results_tsv:
	tsv_reader = csv.DictReader(blast_results_tsv, delimiter="\t")
	for row in tsv_reader:
		if "GUT_GENOME" == row["sseqid"][:10]: # Ignore positive controls
			blast_results.append(Blast_result(row["qseqid"], row["sseqid"], row["pident"], row["evalue"], row["bitscore"], arg_classes, gen_phylums))

with open(output_path, "w", newline="") as tsvfile:
	fieldnames = ["qseqid", "sseqid", "pident", "evalue", "bitscore", "arg_class", "gen_phylum"]
	writer = csv.DictWriter(tsvfile, delimiter="\t", fieldnames=fieldnames)

	writer.writeheader()
	for br in blast_results:
		writer.writerow({"qseqid": br.qseqid, "sseqid": br.sseqid, "pident": br.pident, "evalue": br.evalue, "bitscore": br.bitscore, "arg_class": br.arg_class, "gen_phylum": br.gen_phylum})

# python3 join_blast.py ../../database/megares_v1.01/megares_annotations_v1.01.csv ../../database/genomes_metadata.tsv ../../database/test_genomes/blast_results.tsv | less
