import sys
import csv

file_path = sys.argv[1]
out_path = sys.argv[2]

delimiter = ','
if file_path.split('.')[1] == 'tsv':
    delimiter = '\t'

table = []
with open(file_path, mode='r') as file:
    csv_reader = csv.DictReader(file, delimiter=delimiter)
    fieldnames = csv_reader.fieldnames
    for row in csv_reader:
        table.append(row)

fieldnames.append("phylum")

with open(out_path, "w", newline="") as file:
    csv_writer = csv.DictWriter(file, delimiter="\t", fieldnames=fieldnames)
    csv_writer.writeheader()
    for row in table:
        r = row.copy()
        r["phylum"] = row["gen_lineage"].split(';')[1].split('_')[2]
        csv_writer.writerow(r)
