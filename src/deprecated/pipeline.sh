#!/bin/bash

SRC_DIR=./src
DB_DIR=./data
OUT_DIR=$1

# MAKE OUT_DIR
mkdir $OUT_DIR

# FILTERING ISOLATES
mlr --tsv filter '$Genome_type == "Isolate"' $DB_DIR/genomes_metadata.tsv > $OUT_DIR/isolates.tsv

# DOWNLOAD GENOMIC DATA
mlr --itsv --onidx cut -f FTP_download < $OUT_DIR/isolates.tsv | wget -P $OUT_DIR/genomes -i -
gunzip -rv $OUT_DIR/genomes

# GETTING FASTA DATA AND APPENDING IT TO ONE SINGLE FILE
for f in $(ls $OUT_DIR/genomes); do sed -n '/##FASTA/,$p' < $OUT_DIR/genomes/$f | sed '1d' >> $OUT_DIR/all_genomes.fasta; rm $OUT_DIR/genomes/$f; done
rmdir $OUT_DIR/genomes

# BLAST DATABASE
makeblastdb -in $OUT_DIR/all_genomes.fasta -dbtype nucl

# HEADERS
echo '1iqseqid	sseqid	pident	length	mismatch	gapopen	qstart	qend	sstart	send	evalue	bitscore' > $OUT_DIR/blast_out.tsv

# BLASTN
blastn -query $DB_DIR/megares_v2.00/megares_modified_database_v2.00.fasta -db $OUT_DIR/all_genomes.fasta -outfmt 6 >> $OUT_DIR/blast_out.tsv

# PREPARE FOR JOIN
mlr --tsv put '$sseqid =~ "GUT_GENOME([0-9]+)_([0-9]+)"; $genome = "GUT_GENOME\1"' $OUT_DIR/blast_out.tsv > $OUT_DIR/blast_out_wGenome.tsv

# JOIN
python3 $SRC_DIR/join.py --inline 'c0-b0 c1 c2 c10 c11 "arg_class"b2 "gen_lineage"a17 c12-a0' $OUT_DIR/join_out.tsv $OUT_DIR/isolates.tsv $DB_DIR/megares_v2.00/megares_modified_annotations_v2.00.csv $OUT_DIR/blast_out_wGenome.tsv

# EXTRACT PHYLUM
mlr --tsv put '$gen_lineage =~ ".*;p__([a-zA-Z]+).*"; $phylum = "\1"' $OUT_DIR/join_out.tsv > $OUT_DIR/join_out_wPhylum.tsv

# HEATMAP
Rscript $SRC_DIR/heatmap.r $OUT_DIR/join_out_wPhylum.tsv
