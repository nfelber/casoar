#!/usr/bin/env Rscript
library(dplyr)
library(tidyr)
library(ggplot2)
library(ggdendro)
library(grid)
library(gridExtra)
library(viridis)

args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (1 != length(args)) {
	  stop("Usage: hitmap.r <input-file>", call.=FALSE)
}

file <- args[1]

#file <- "~/maturity_project/database/test_100/tmp.tsv"

data <- read.delim(file) %>% as_tibble() # read data

# VERTICAL DENDROGRAM
v_table_matrix <- as.matrix(table(data$arg_class, data$phylum))
#for(i in 1:ncol(v_table_matrix)) {
#  colnames(v_table_matrix)[i] <- paste(colnames(v_table_matrix)[i], colSums(v_table_matrix)[i])
#}
v_dendro <- as.dendrogram(hclust(d = dist(x = v_table_matrix)))

v_dendro.plot <- ggdendrogram(data = v_dendro, rotate = TRUE) + theme(axis.text.y = element_blank())

# HORIZONTAL DENDROGRAM
h_table_matrix <- t(v_table_matrix)
h_dendro <- as.dendrogram(hclust(d = dist(x = h_table_matrix)))

h_dendro.plot <- ggdendrogram(data = h_dendro) + theme(axis.text.x = element_blank())

# HEATMAP
c <- count(data, arg_class, phylum, .drop=FALSE) #.drop=FALSE prevents keeps empty cells in the output of count()

c <- spread(c, phylum, n)
#for(i in 2:ncol(c)) {
#  names(c)[i] <- paste(names(c)[i], sum(c[names(c)[i]]))
#}
for(cn in colnames(c)[2:ncol(c)]) {
  c[cn] <- c[cn]/sum(c[cn])
}
c <- gather(c, phylum, n, -arg_class)

c.v_order = order.dendrogram(v_dendro) # obtain the order from dendrogram
c.h_order = order.dendrogram(h_dendro)
c$arg_class <- factor(x = c$arg_class, levels = row.names(v_table_matrix)[c.v_order], ordered = TRUE)
c$phylum <- factor(x = c$phylum, levels = row.names(h_table_matrix)[c.h_order], ordered = TRUE)

# spread(c, phylum, n)
# gather(c, phylum, n, -arg_class)

c.plot = ggplot(c, aes(x=phylum, y=arg_class, fill=n)) + geom_tile() +
  scale_fill_viridis(option = "viridis") + theme(legend.position = "left", axis.text.x = element_text(angle=60, hjust=1)) # fill=n/sum(n)

# "magma"

# GRID
pdf("heatmap.pdf", width=16, height=12)
grid.newpage()
print(c.plot, vp = viewport(x = 0.4, y = 0.4, width = 0.8, height = 0.8)) # x and y coordinates refer to the middle point of the viewport
print(v_dendro.plot, vp = viewport(x = 0.9, y = 0.45, width = 0.2, height = 0.75))
print(h_dendro.plot, vp = viewport(x = 0.58, y = 0.9, width = 0.44, height = 0.2))
dev.off()

# ARRANGE
grid.arrange(c.plot, h_dendro.plot, v_dendro.plot, widths = c(1, 1, 1), heights = c(1, 4), layout_matrix = rbind(c(NA, 2, NA), c(1, 1, 3)))
