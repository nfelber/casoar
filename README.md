# Casoar

## Objectives

To find antibiotic resistance genes (ARGs) in various bacterial genomes.

See the [specs](docs/specs.md) for details.

## Status

(2019-10-24) Project is ongoing and no major diffculties have been encountered or foreseen.

See the [log](docs/log.md) and [TODO](TODO.md) for details.

## Directory Structure

* `src` - sources
* `data` - genomes, ARGs (ignored by Git)
* `doc` - internal documentation, as well scientific papers
* `test` - test procedures

## Milestones

1. download the data (genomes, ARGs, ...)
2. install the software (BLAST)
3. write scripts for automated searching of ARGs in the genomes, and parsing of
   the results
4. (?) ditto for antibiotic-producing genes
5. (?) write code for graphical representation

also see [TODO](TODO.md) for the gory details

## Misc
