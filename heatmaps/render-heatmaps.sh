#!/bin/bash

SRCDIR=../src
OUTDIR=~/casoar-out

for t in $(mlr --icsv --onidx uniq -f arg_type $OUTDIR/arg_counts.csv); do
	Rscript $SRCDIR/heatmap3.r $OUTDIR/arg_counts_$t.csv $t.pdf;
	convert -density 150 $t.pdf -quality 90 $t.png;
done

Rscript $SRCDIR/heatmap3.r $OUTDIR/arg_counts.csv heatmap.pdf;
convert -density 150 heatmap.pdf -quality 90 heatmap.png;
